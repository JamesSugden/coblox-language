macro(CL_INCLUDE_CL_STDLIB t)
	target_include_directories(${t} PRIVATE "${PROJECT_SOURCE_DIR}/libs/CL_StdLib" SYSTEM INTERFACE "${PROJECT_SOURCE_DIR}/libs/CL_StdLib")
endmacro()
