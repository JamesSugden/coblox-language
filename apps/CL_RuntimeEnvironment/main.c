#include <stdio.h>
#include <stdlib.h>
#include "core/core.h"

int main(void)
{
	Core * c = alloc_core();
	load_object_file(c, (CL_BYTE_CODE_EXAMPLE1));
	execute(c);
	fflush(stdin);
	printf("\n"
		"----------------------------------------------------------------------------------\n"
		"-- Execution finished, press any key to continue\n"
		"----------------------------------------------------------------------------------");
	(void)getchar();
	free_core(c);
	return 0;
}
