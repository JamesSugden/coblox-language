#include "parser.h"

struct Parser {
	char * obj_code;
	uint64_t obj_code_size;	// Total number of bytes in obj_code (excluding null terminator)
	uint64_t buffer_index;	// Index into the obj_code buffer of the start of the next line
	PCPTR next_line;		// Index into the theoretical array of lines of the next line
	PCPTR num_lines;		// The total number of lines of object code
	struct BlockRegistry * block_registry;	// Block registry will be fully initialised after all instructions have been parsed
};



// Calculate the number of lines of object code
static void calc_num_lines(Parser * p);

// Gets the next line of code to execute
// Sets the value of c to the next line
// Sets the value at eof to 1 if the next line is also the last
static void next_line(Parser * p, char line[MAX_LINE_LENGTH], int * eof);

// Parse a line and set an instruction object
static void parse_line(Parser * p, char line[MAX_LINE_LENGTH], Instruction * in);

// Parse function for every type of instruction
static inline void parse_inst_block(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_alloc(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_end_1(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_end_2(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_bstore_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_free(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);

static inline void parse_inst_branch(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_branch_i_if_z(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_branch_i_if_zp(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_call(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_call_store_v(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_call_std(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in);
static inline void parse_inst_call_std_store_v(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in);

static inline void parse_inst_return(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_return_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);

static inline void parse_inst_store_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_store_s(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_load_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);

static inline void parse_inst_and_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_or_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_xor_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_not_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_bnot_i(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in);

static inline void parse_inst_add_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_sub_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);

static inline void parse_inst_store_get_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);

static inline void parse_inst_print_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);
static inline void parse_inst_print_s(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in);



// Allocate memory for a new parser object
Parser * alloc_parser(char * obj_code, struct BlockRegistry * block_registry)
{
	if(obj_code == NULL || block_registry == NULL) {
		return NULL;
	}
	Parser * p = malloc(sizeof *p);
	if(p) {
		p->obj_code_size = strlen(obj_code);
		p->obj_code = obj_code;
		p->buffer_index = 0;
		p->next_line = 0;
		p->block_registry = block_registry;
		calc_num_lines(p);
	}
	return p;
}

// Free a parser object
void free_parser(Parser * p)
{
	// NOTE - Do not free obj_code since p is not the owner
	free(p);
}

// Calculate the number of lines of object code
static void calc_num_lines(Parser * p)
{
	uint64_t num_lines = 1;
	if(p->obj_code) {
		// Search for the end of the line
		for(uint64_t i = 0; i < p->obj_code_size; i++) {
			char c = p->obj_code[i];
			if(c == '\n' || c == '\0') {
				num_lines ++;
			}
		}
	}
	p->num_lines = num_lines;
}

// Get the number of lines in the parsed file
PCPTR get_num_parsed_lines(Parser * p)
{
	return p->num_lines;
}

// Get the next instruction to execute
// Sets the value of i to the next instruction
// Sets instruction type to EOF if the end of the file is reached
void parse_next_instruction(Parser * p, Instruction * i)
{
	if(p && i) {
		// Get the next line of code to execute
		char line[MAX_LINE_LENGTH];
		int eof = 0;
		next_line(p, line, &eof);

		if(eof) {
			i->type = itEOF;
		} else {
	///		printf("%llu: %s \n", p->prog_counter, line);
			// Parse the line and set the instruction object
			parse_line(p, line, i);
		}

		// TODO - Ensure this is set correctly when jumps are added
		p->next_line ++;
	}
}

// Gets the next line of code to execute
// Sets the value of c to the next line
// Sets the value at eof to 1 if the next line is also the last
static void next_line(Parser * p, char line[MAX_LINE_LENGTH], int * eof)
{
	// Length of the next line (excluding its line feed)
	uint64_t line_length = 0;

	// Detect if the end of the file has been reached
	if(p->obj_code_size <= p->buffer_index) {
		*eof = 1;
		return;
	}

	// Search for the end of the line
	for(uint64_t i = 0; i < MAX_LINE_LENGTH; i++) {
		char c = p->obj_code[p->buffer_index + i];
		if(c == '\n' || c == '\0') {
			line_length = i;
			break;
		}
	}

	// Copy from the obj_code buffer into the line buffer
	memcpy_s(line, MAX_LINE_LENGTH-1, p->obj_code+p->buffer_index, line_length);
	line[line_length] = '\0';

	// Advance the buffer index to the start of the next line
	p->buffer_index = p->buffer_index + line_length + 1;
}

// Parse a line and set an instruction object
static void parse_line(Parser * p, char line[MAX_LINE_LENGTH], Instruction * in)
{
	// Split the line into space separated tokens
	char * tokens[MAX_NUM_TOKENS_PER_LINE];
	int j = 0;
	char * token;
	token = strtok(line, " ");
	while(token != NULL && j < MAX_NUM_TOKENS_PER_LINE) {
///		printf("  Token %s \n", token);
		tokens[j] = token;
		j ++;
		token = strtok(NULL, " ");
	}

	// If there is no first token, set the instruction to empty s.t. it can be ignored
	if(j == 0) {
		in->type = itEMPTY;
		return;
	}

	// Use the first token to determine the instruction type
	if(strcmp(tokens[0], "BLOCK") == 0) {
		parse_inst_block(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "END_1") == 0) {
		parse_inst_end_1(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "END_2") == 0) {
		parse_inst_end_2(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "ALLOC") == 0) {
		parse_inst_alloc(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "BSTORE_I") == 0) {
		parse_inst_bstore_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "FREE") == 0) {
		parse_inst_free(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "BRANCH") == 0) {
		parse_inst_branch(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "BRANCH_I_IF_Z") == 0) {
		parse_inst_branch_i_if_z(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "BRANCH_I_IF_ZP") == 0) {
		parse_inst_branch_i_if_zp(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "CALL") == 0) {
		parse_inst_call(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "CALL_STORE_V") == 0) {
		parse_inst_call_store_v(p, tokens, j-1, in);
	} else if (strcmp(tokens[0], "CALL_STD") == 0) {
		parse_inst_call_std(p, tokens, j - 1, in);
	} else if (strcmp(tokens[0], "CALL_STD_STORE_V") == 0) {
		parse_inst_call_std_store_v(p, tokens, j - 1, in);
	} else if(strcmp(tokens[0], "RETURN") == 0) {
		parse_inst_return(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "RETURN_I") == 0) {
		parse_inst_return_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "STORE_I") == 0) {
		parse_inst_store_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "STORE_S") == 0) {
		parse_inst_store_s(p, tokens, j-1, in);
	} else if (strcmp(tokens[0], "LOAD_I") == 0) {
		parse_inst_load_i(p, tokens, j - 1, in);
	} else if(strcmp(tokens[0], "AND_I") == 0) {
		parse_inst_and_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "OR_I") == 0) {
		parse_inst_or_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "XOR_I") == 0) {
		parse_inst_xor_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "NOT_I") == 0) {
		parse_inst_not_i(p, tokens, j-1, in);
	} else if (strcmp(tokens[0], "BNOT_I") == 0) {
		parse_inst_bnot_i(p, tokens, j - 1, in);
	} else if(strcmp(tokens[0], "ADD_I") == 0) {
		parse_inst_add_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "SUB_I") == 0) {
		parse_inst_sub_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "STORE_GET_I") == 0) {
		parse_inst_store_get_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "PRINT_I") == 0) {
		parse_inst_print_i(p, tokens, j-1, in);
	} else if(strcmp(tokens[0], "PRINT_S") == 0) {
		parse_inst_print_s(p, tokens, j-1, in);
	} else {
		in->type = itERROR;
	}
}

// Parse a BLOCK instruction (marks the start of a code block)
static inline void parse_inst_block(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// BLOCK requires 2 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itBLOCK;
		in->BLOCK_ID = strtoull(tokens[1], NULL, 10);
		// Register the enter line of the block as the line of the block instruction
		register_block_enter_line(p->block_registry, in->BLOCK_ID, p->next_line);
	}
}

// Parse an ALLOC instruction (sets stack frame storage and possibly heap frame storage for a code block)
static inline void parse_inst_alloc(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// ALLOC takes either 1 or 2 arguments
	if(num_args == 1) {
		in->type = itALLOC_1;
		in->ALLOC_1_STACK_FRAME_SIZE = strtoull(tokens[1], NULL, 10);
	} else if(num_args == 2) {
		in->type = itALLOC_2;
		in->ALLOC_2_STACK_FRAME_SIZE = strtoull(tokens[1], NULL, 10);
		in->ALLOC_2_HEAP_OBJECT_SIZE = strtoull(tokens[2], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

// Parse an END_1 instruction (marks the end of a code block allocated with ALLOC <u>)
// An END_1 instruction acts a return statement with void as the argument
static inline void parse_inst_end_1(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// END_1 requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itEND_1;
		in->END_ID = strtoull(tokens[1], NULL, 10);
		// Register the exit line of the block as the line of the end instruction
		register_block_exit_line(p->block_registry, in->END_ID, p->next_line);
	}
}

// Parse an END_2 instruction (marks the end of a code block allocated with ALLOC <u> <u>)
// An END_2 instruction acts a return statement with the block itself as the argument
static inline void parse_inst_end_2(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// END_2 requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itEND_2;
		in->END_ID = strtoull(tokens[1], NULL, 10);
		// Register the exit line of the block as the line of the end instruction
		register_block_exit_line(p->block_registry, in->END_ID, p->next_line);
	}
}

// Parse a BSTORE_I instruction (stores a 32-bit integer in block storage)
static inline void parse_inst_bstore_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// BSTORE_I requires 2 arguments
	if(num_args != 2) {
		in->type = itERROR;
	} else {
		in->type = itBSTORE_I;
		in->BSTORE_I_OFFSET = strtoull(tokens[1], NULL, 10);
		in->BSTORE_I_VALUE = strtol(tokens[2], NULL, 10);
	}
}

// Parse a FREE instruction (frees the object by the object pointer at the stack frame offset)
static inline void parse_inst_free(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// FREE requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itFREE;
		in->FREE_STACK_FRAME_BLOCK_ID_OFFSET = strtoull(tokens[1], NULL, 10);
	}
}

// Parse a BRANCH instruction (branches by a relative number of lines)
static inline void parse_inst_branch(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// BRANCH requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itBRANCH;
		in->BRANCH_JUMP_VALUE = (SPCOFFSET)strtol(tokens[1], NULL, 10);
	}
}

// Parse a BRANCH_IF_Z instruction (branches by a relative number of lines if the integer at offset is zero)
static inline void parse_inst_branch_i_if_z(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// BRANCH_IF_Z requires 2 arguments
	if(num_args != 2) {
		in->type = itERROR;
	} else {
		in->type = itBRANCH_I_IF_Z;
		in->BRANCH_I_JUMP_VALUE = (SPCOFFSET)strtol(tokens[1], NULL, 10);
		in->BRANCH_I_VALUE_OFFSET = strtol(tokens[2], NULL, 10);
	}
}

static inline void parse_inst_branch_i_if_zp(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// BRANCH_IF_ZP requires 2 arguments
	if(num_args != 2) {
		in->type = itERROR;
	} else {
		in->type = itBRANCH_I_IF_ZP;
		in->BRANCH_I_JUMP_VALUE = (SPCOFFSET)strtol(tokens[1], NULL, 10);
		in->BRANCH_I_VALUE_OFFSET = strtol(tokens[2], NULL, 10);
	}
}

// Parse a CALL instruction (branches unconditionally to a code block)
static inline void parse_inst_call(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// CALL requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itCALL;
		in->CALL_BLOCK_ID = strtoull(tokens[1], NULL, 10);
	}
}

// Parse a CALL_STORE_V instruction (branches unconditionally to a code block and stores the return value in the stack frame)
static inline void parse_inst_call_store_v(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// CALL_STORE_V requires 2 arguments
	if(num_args != 2) {
		in->type = itERROR;
	} else {
		in->type = itCALL_STORE_V;
		in->CALL_STORE_V_BLOCK_ID = strtoull(tokens[1], NULL, 10);
		in->CALL_STORE_V_OFFSET = strtoull(tokens[2], NULL, 10);
	}
}

// Parse a CALL_STD instruction (call a standard library function)
static inline void parse_inst_call_std(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in)
{
	// CALL_STD requires 1 argument
	if (num_args != 1) {
		in->type = itERROR;
	}
	else {
		in->type = itCALL_STD;
		in->CALL_STD_FUNC_ID = strtoull(tokens[1], NULL, 10);
	}
}

// Parse a CALL_STD_STORE_V instruction (call a standard library function and store the return value)
static inline void parse_inst_call_std_store_v(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in)
{
	// CALL_STD_STORE_V requires 2 arguments
	if (num_args != 2) {
		in->type = itERROR;
	}
	else {
		in->type = itCALL_STD_STORE_V;
		in->CALL_STD_STORE_V_FUNC_ID = strtoull(tokens[1], NULL, 10);
		in->CALL_STD_STORE_V_OFFSET = strtoull(tokens[2], NULL, 10);
	}
}

// Parse a RETURN instruction
static inline void parse_inst_return(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// Returns requires no arguments
	if(num_args != 0) {
		in->type = itERROR;
	} else {
		in->type = itRETURN;
	}
}

// Parse a RETURN_I instruction
static inline void parse_inst_return_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// Returns requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itRETURN_I;
		in->RETURN_I_OFFSET = strtoull(tokens[1], NULL, 10);
	}
}

// Parse a STORE_I instruction (stores value in the stack frame of the current scope)
static inline void parse_inst_store_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// STORE_I requires 2 arguments
	if(num_args != 2) {
		in->type = itERROR;
	} else {
		in->type = itSTORE_I;
		in->STORE_I_OFFSET = strtoull(tokens[1], NULL, 10);
		in->STORE_I_VALUE = strtol(tokens[2], NULL, 10);
	}
}

// Parse a STORE_S instruction (creates a string object and stores ptr in the stack frame of the current scope)
static inline void parse_inst_store_s(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// STORE_I requires at least 2 arguments
	if(num_args < 2) {
		in->type = itERROR;
	} else {
		in->type = itSTORE_S;
		in->STORE_S_OFFSET = strtoull(tokens[1], NULL, 10);
		// Calculate the size of the string by appending all tokens after the offset together
		SPTR size = 0;
		for(int i = 2; i <= num_args; i++) {
			// Add the length of the string plus the space
			size += strlen(tokens[i]) + 1;
		}
		// Allocate memory for the string
		char * string = calloc((size_t)size+1, sizeof *string);
		if (string == NULL) {
			in->type = itERROR;
			return;
		}
		// Copy the data into the string
		for(int i = 2; i <= num_args; i++) {
			strcat(string, tokens[i]);
			strcat(string, " ");
		}
		string[size-1] = '\0';
		// Set the instruction value
		in->STORE_S_STRING = string;
		in->STORE_S_STRING_SIZE = size;
	}
}

// Parse a LOAD_I instruction (loads a 32-bit integer from block storage and stores in the current stack frame)
static inline void parse_inst_load_i(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in)
{
	// BLOAD_I requires 3 arguments
	if (num_args != 3) {
		in->type = itERROR;
	}
	else {
		in->type = itLOAD_I;
		in->LOAD_I_STACK_FRAME_BLOCK_ID_OFFSET = strtoull(tokens[1], NULL, 10);
		in->LOAD_I_BLOCK_OFFSET = (SPTR)strtol(tokens[2], NULL, 10);
		in->LOAD_I_STACK_FRAME_OFFSET = strtoull(tokens[3], NULL, 10);
	}
}

static inline void parse_inst_and_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// AND_I takes 3 arguments
	if (num_args == 3) {
		in->type = itAND_I;
		in->AND_I_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->AND_I_OFFSET_2 = strtoull(tokens[2], NULL, 10);
		in->AND_I_OFFSET_3 = strtoull(tokens[3], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

static inline void parse_inst_or_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// OR_I takes 3 arguments
	if (num_args == 3) {
		in->type = itOR_I;
		in->OR_I_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->OR_I_OFFSET_2 = strtoull(tokens[2], NULL, 10);
		in->OR_I_OFFSET_3 = strtoull(tokens[3], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

static inline void parse_inst_xor_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// XOR_I takes 3 arguments
	if (num_args == 3) {
		in->type = itXOR_I;
		in->XOR_I_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->XOR_I_OFFSET_2 = strtoull(tokens[2], NULL, 10);
		in->XOR_I_OFFSET_3 = strtoull(tokens[3], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

static inline void parse_inst_not_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// NOT_I takes 2 arguments
	if (num_args == 2) {
		in->type = itNOT_I;
		in->NOT_I_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->NOT_I_OFFSET_2 = strtoull(tokens[2], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

static inline void parse_inst_bnot_i(Parser* p, char* tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction* in)
{
	// BNOT_I takes 2 arguments
	if (num_args == 2) {
		in->type = itBNOT_I;
		in->BNOT_I_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->BNOT_I_OFFSET_2 = strtoull(tokens[2], NULL, 10);
	}
	else {
		in->type = itERROR;
	}
}

// Parse an ADD_I instruction
static inline void parse_inst_add_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// ADD_I takes either 2 or 3 arguments
	if(num_args == 2) {
		in->type = itADD_I_2;
		in->ADD_I_2_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->ADD_I_2_OFFSET_2 = strtoull(tokens[2], NULL, 10);
	} else if (num_args == 3) {
		in->type = itADD_I_3;
		in->ADD_I_3_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->ADD_I_3_OFFSET_2 = strtoull(tokens[2], NULL, 10);
		in->ADD_I_3_OFFSET_3 = strtoull(tokens[3], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

// Parse a SUB_I instruction
static inline void parse_inst_sub_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// SUB_I takes either 2 or 3 arguments
	if(num_args == 2) {
		in->type = itSUB_I_2;
		in->SUB_I_2_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->SUB_I_2_OFFSET_2 = strtoull(tokens[2], NULL, 10);
	} else if (num_args == 3) {
		in->type = itSUB_I_3;
		in->SUB_I_3_OFFSET_1 = strtoull(tokens[1], NULL, 10);
		in->SUB_I_3_OFFSET_2 = strtoull(tokens[2], NULL, 10);
		in->SUB_I_3_OFFSET_3 = strtoull(tokens[3], NULL, 10);
	} else {
		in->type = itERROR;
	}
}

// Parse a STORE_GET_I instruction (gets next 32-bit integer input from command line and stores value in the stack frame of the current scope)
static inline void parse_inst_store_get_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// STORE_GET_I requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itSTORE_GET_I;
		in->STORE_GET_I_OFFSET = strtoull(tokens[1], NULL, 10);
	}
}

// Parse a PRINT_I instruction
static inline void parse_inst_print_i(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// PRINT_I requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itPRINT_I;
		in->PRINT_I_OFFSET = strtoull(tokens[1], NULL, 10);
	}
}

// Parse a PRINT_S instruction
static inline void parse_inst_print_s(Parser * p, char * tokens[MAX_NUM_TOKENS_PER_LINE], int num_args, Instruction * in)
{
	// PRINT_S requires 1 argument
	if(num_args != 1) {
		in->type = itERROR;
	} else {
		in->type = itPRINT_S;
		in->PRINT_S_OFFSET = strtoull(tokens[1], NULL, 10);
	}
}
