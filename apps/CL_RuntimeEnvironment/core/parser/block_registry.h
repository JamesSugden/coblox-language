#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "core/types.h"

struct BlockAddress {
	PCPTR block_enter_line;		// Index of the line of the BLOCK instruction
	PCPTR block_exit_line;		// Index of the line of the END instruction
};

struct BlockRegistry {
	struct BlockAddress * block_addresses;
	uint32_t num_blocks;
};

// Allocate memory for a new block registry object
struct BlockRegistry * alloc_block_registry();

// Free a block registry object
void free_block_registry(struct BlockRegistry * b);

// Register the enter line of a block
void register_block_enter_line(struct BlockRegistry * b, ID block_id, PCPTR enter_line);

// Register the entry line of a block with the registry
void register_block_exit_line(struct BlockRegistry * b, ID block_id, PCPTR exit_line);
