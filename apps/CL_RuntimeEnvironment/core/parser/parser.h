#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "instruction.h"
#include "block_registry.h"

#define MAX_LINE_LENGTH 128
#define MAX_NUM_TOKENS_PER_LINE 10

typedef struct Parser Parser;

// Allocate memory for a new parser object
Parser * alloc_parser(char * obj_code, struct BlockRegistry * block_registry);

// Free a parser object
void free_parser(Parser * p);

// Get the number of lines in the parsed file
PCPTR get_num_parsed_lines(Parser * p);

// Get the next instruction to execute
// Sets the value of i to the next instruction
// Sets instruction type to EOF if the end of the file is reached
void parse_next_instruction(Parser * p, Instruction * i);
