#include "block_registry.h"
#include "core/error.h"

#define DEFAULT_NUM_BLOCKS 64;

// Resize the block addresses array to double its size
static inline void resize_block_addresses(struct BlockRegistry * b, size_t required_size);



// Allocate memory for a new block registry object
struct BlockRegistry * alloc_block_registry()
{
	struct BlockRegistry * b = malloc(sizeof *b);
	if(b) {
		b->num_blocks = DEFAULT_NUM_BLOCKS;
		b->block_addresses = calloc(b->num_blocks, sizeof *b->block_addresses);

		if(!b->block_addresses) {
			free(b);
			return NULL;
		}
	}
	return b;
}

// Free a block registry object
void free_block_registry(struct BlockRegistry * b)
{
	if(b) {
		free(b->block_addresses);
	}
	free(b);
}

// Register the enter line of a block with the registry
void register_block_enter_line(struct BlockRegistry * b, ID block_id, PCPTR enter_line)
{
	// Registry stores the block address in an array at index block_id
	// Therefore, if block_id is larger than array size, array is too small
	if(block_id >= b->num_blocks) {
		resize_block_addresses(b, block_id);
	}

	// Set the block address
	b->block_addresses[block_id].block_enter_line = enter_line;
}

// Register the entry line of a block with the registry
void register_block_exit_line(struct BlockRegistry * b, ID block_id, PCPTR exit_line)
{
	// Registry stores the block address in an array at index block_id
	// Therefore, if block_id is larger than array size, array is too small
	if(block_id >= b->num_blocks) {
		resize_block_addresses(b, block_id);
	}

	// Set the block address
	b->block_addresses[block_id].block_exit_line = exit_line;
}

// Resize the block addresses array to double its size
static inline void resize_block_addresses(struct BlockRegistry * b, size_t required_size)
{
	while(b->num_blocks < required_size) {
		b->num_blocks *= 2;
	}
	b->block_addresses = realloc(b->block_addresses, b->num_blocks);
	// If the realloc failed, exit the program with an error
	if(b->block_addresses == NULL) {
		on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to re-allocate the block registry addresses array", 0);
	}
}
