#pragma once
#include "core/types.h"

typedef enum {
	itSOF,
	itEOF,
	itERROR,
	itEMPTY,

	itBLOCK,
	itALLOC_1,
	itALLOC_2,
	itEND_1,
	itEND_2,
	itBSTORE_I,
	//itBLOAD_I,
	itFREE,

	itBRANCH,
	itBRANCH_I_IF_Z,
	itBRANCH_I_IF_ZP,
	itCALL,
	itCALL_STORE_V,
	itCALL_STD,
	itCALL_STD_STORE_V,

	itRETURN,
	itRETURN_I,

	itSTORE_I,
	itSTORE_S,
	itLOAD_I,

	itAND_I,
	itOR_I,
	itXOR_I,
	itNOT_I,
	itBNOT_I,

	itADD_I_2,
	itADD_I_3,
	itSUB_I_2,
	itSUB_I_3,

	itSTORE_GET_I,

	itPRINT_I,
	itPRINT_S
} InstructionType;

typedef struct {
	InstructionType type;
	union {
		// Data for the BLOCK instruction
		struct {
			ID BLOCK_ID;
		};

		// Data for the ALLOC_1 instruction
		struct {
			PTR ALLOC_1_STACK_FRAME_SIZE;
		};
		// Data for the ALLOC_2 instruction
		struct {
			PTR ALLOC_2_STACK_FRAME_SIZE;
			PTR ALLOC_2_HEAP_OBJECT_SIZE;
		};

		// Data for the END_1 and END_2 instructions
		struct {
			ID END_ID;
		};

		// Data for the BSTORE_I instruction
		struct {
			PTR BSTORE_I_OFFSET;
			uint32_t BSTORE_I_VALUE;
		};

		// Data for the FREE instruction
		struct {
			PTR FREE_STACK_FRAME_BLOCK_ID_OFFSET;
		};

		// Data for the BRANCH instruction
		struct {
			SPCOFFSET BRANCH_JUMP_VALUE;
		};

		// Data for the BRANCH_I instruction group (BRANCH_I_IF_Z, BRANCH_I_IF_ZP)
		struct {
			SPCOFFSET BRANCH_I_JUMP_VALUE;
			int32_t BRANCH_I_VALUE_OFFSET;
		};

		// Data for the CALL instruction
		struct {
			ID CALL_BLOCK_ID;
		};

		// Data for the CALL_STORE_V instruction
		struct {
			ID CALL_STORE_V_BLOCK_ID;
			PTR CALL_STORE_V_OFFSET;
		};

		// Data for the CALL_STD instruction
		struct {
			ID CALL_STD_FUNC_ID;
		};

		// Data for the CALL_STD_STORE_V instruction
		struct {
			ID CALL_STD_STORE_V_FUNC_ID;
			PTR CALL_STD_STORE_V_OFFSET;
		};

		// Data for the RETURN_I instruction
		struct {
			PTR RETURN_I_OFFSET;
		};

		// Data for the STORE_I instruction
		struct {
			PTR STORE_I_OFFSET;
			uint32_t STORE_I_VALUE;
		};

		// Data for the STORE_S instruction
		struct {
			PTR STORE_S_OFFSET;
			char * STORE_S_STRING;
			SPTR STORE_S_STRING_SIZE;
		};

		// Data for the LOAD_I instruction
		struct {
			PTR LOAD_I_STACK_FRAME_BLOCK_ID_OFFSET;
			SPTR LOAD_I_BLOCK_OFFSET;
			PTR LOAD_I_STACK_FRAME_OFFSET;
		};

		// Data for the AND_I instruction
		struct {
			PTR AND_I_OFFSET_1;
			PTR AND_I_OFFSET_2;
			PTR AND_I_OFFSET_3;
		};

		// Data for the OR_I instruction
		struct {
			PTR OR_I_OFFSET_1;
			PTR OR_I_OFFSET_2;
			PTR OR_I_OFFSET_3;
		};

		// Data for the XOR_I instruction
		struct {
			PTR XOR_I_OFFSET_1;
			PTR XOR_I_OFFSET_2;
			PTR XOR_I_OFFSET_3;
		};

		// Data for the NOT_I instruction
		struct {
			PTR NOT_I_OFFSET_1;
			PTR NOT_I_OFFSET_2;
		};

		// Data for the BNOT_I instruction
		struct {
			PTR BNOT_I_OFFSET_1;
			PTR BNOT_I_OFFSET_2;
		};

		// Data for the ADD_I_2 (2 argument version of ADD_I) instruction
		struct {
			PTR ADD_I_2_OFFSET_1;
			PTR ADD_I_2_OFFSET_2;
		};
		// Data for the ADD_I_3 (3 argument version of ADD_I) instruction
		struct {
			PTR ADD_I_3_OFFSET_1;
			PTR ADD_I_3_OFFSET_2;
			PTR ADD_I_3_OFFSET_3;
		};

		// Data for the SUB_I_2 (2 argument version of SUB_I) instruction
		struct {
			PTR SUB_I_2_OFFSET_1;
			PTR SUB_I_2_OFFSET_2;
		};

		// Data for the SUB_I_3 (3 argument version of SUB_I) instruction
		struct {
			PTR SUB_I_3_OFFSET_1;
			PTR SUB_I_3_OFFSET_2;
			PTR SUB_I_3_OFFSET_3;
		};

		// Data for the STORE_GET_I instruction
		struct {
			PTR STORE_GET_I_OFFSET;
		};

		// Data for the PRINT_I instruction
		struct {
			PTR PRINT_I_OFFSET;
		};

		// Data for the PRINT_S instruction
		struct {
			PTR PRINT_S_OFFSET;
		};
	};
} Instruction;
