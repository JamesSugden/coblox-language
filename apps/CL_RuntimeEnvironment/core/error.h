#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "types.h"

enum ErrorType {
	// Internal runtime errors
	etINTERNAL_MEMORY_ERROR,
	etPARSE_ERROR,

	// Application errors
	etAPPLICATION_MEMORY_ERROR,
	etINVALID_INSTRUCTION
};

// Called when an un-handleable error occurs
// After error has been logged, the process exits
void on_fatal_error(enum ErrorType type, char * msg, PCPTR line_num);
