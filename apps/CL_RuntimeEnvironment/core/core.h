#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "file_system/file_util.h"
#include "parser/parser.h"
#include "parser/instruction.h"
#include "runtime/runtime.h"

typedef struct Core Core;

// Create a new Core object
Core * alloc_core();

// Free a Core object
void free_core(Core * c);

// Execute the the current object file until it crashes or exits
void execute(Core * c);

// Load a Coblox object file
void load_object_file(Core * c, char * file_path);
