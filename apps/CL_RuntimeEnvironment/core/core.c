#include "core.h"
#include "error.h"

struct Core {
	struct File * obj_code_file;
};

// Create a new Core object
Core * alloc_core()
{
	Core * c = malloc(sizeof *c);
	if(c) {
		c->obj_code_file = NULL;
	}
	return c;
}

// Free a Core object
void free_core(Core * c)
{
	if(c) {
		free_file(c->obj_code_file);
	}
	free(c);
}

// Execute the the current object file until it crashes or exits
void execute(Core * c)
{
	if(c && c->obj_code_file) {
		// Create a block registry object
		struct BlockRegistry * b = alloc_block_registry();

		// Create a parser object
		Parser * p =  alloc_parser(c->obj_code_file->contents, b);

		// Create a runtime object
		Runtime * r = alloc_runtime(b);

		// Log the start of the execution
		printf("----------------------------------------------------------------------------------\n");
		printf("-- Executing %s\n", c->obj_code_file->path);
		printf("----------------------------------------------------------------------------------\n\n");

		// Create an array of every instruction in the object file
		PCPTR num_instructions = get_num_parsed_lines(p);
		Instruction * instructions = calloc(num_instructions, sizeof *instructions);
		if(!instructions) {
			on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to allocate instructions array", 0);
			return;
		}

		// Parse each line and store as an instruction object
		for(uint64_t i = 0; i < num_instructions; ++i) {
			parse_next_instruction(p, &instructions[i]);
		}
		printf("Num instructions generated: %llu\n\n", num_instructions);

		// Execute the code starting from 
		exec_program(r, instructions, num_instructions);

		// De-allocate memory
		free_block_registry(b);
		free_parser(p);
		free_runtime(r);
	}
}

// Load a Coblox object file
void load_object_file(Core * c, char * file_path)
{
	if(c && file_path) {
		// Free the current file if there is one
		if(c->obj_code_file) {
			free_file(c->obj_code_file);
		}

		// Load the new file
		struct File * f = load_file(file_path);
		if(f) {
			c->obj_code_file = f;
		} else {
			printf("Failed to load Coblox object file, file not found: %s", file_path);
		}
		//printf(f->contents);
		//printf("\n");
		//printf(f->path);
	}
}
