#include "file_util.h"

#define MAX_READ_LENGTH 1000

// Load a file from the file system
// TODO - Add error reporting/returning
struct File * load_file(char * path)
{
	if(!path) return NULL;

	FILE * fp;
	
	// Open the file
	fopen_s(&fp, path, "r");
	if(!fp) return NULL;

	// Get the size of the file
	fseek(fp, 0L, SEEK_END);
	uint64_t file_size = ftell(fp);

	// Reset the file pointer to the first byte
	fseek(fp, 0L, SEEK_SET);

	// Allocate memory for the buffer
	char * contents = calloc(file_size+1, sizeof *contents);
	if(!contents) return NULL;

	// Read the file contents into the buffer
	fread(contents, sizeof *contents, file_size, fp);
	contents[file_size] = '\0';

	// Close the file pointer
	fclose(fp);

	// Allocate memory needed for the File object
	struct File * f = malloc(sizeof *f);
	size_t path_length = strlen(path);
	char * path_copy = calloc(path_length+1, sizeof *path_copy);

	// If any of the memory allocation failed, free all of it and return NULL
	if(!f || !path_copy) {
		free(f);
		free(contents);
		free(path_copy);
		return NULL;
	}

	// Copy path into path_copy buffer
	strcpy_s(path_copy, path_length+1, path);
	path_copy[path_length] = '\0';

	// Set the fields of f
	f->path = path_copy;
	f->contents = contents;

	// Return the file object
	return f;
}

// Free a file object
void free_file(struct File * f)
{
	if(f) {
		free(f->path);
		free(f->contents);
	}
	free(f);
}
