#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

struct File {
	char * path;
	char * contents;
};

// Load a file from the file system
struct File * load_file(char * path);

// Free a file object
void free_file(struct File * f);
