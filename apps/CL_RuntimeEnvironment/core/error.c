#include "error.h"

// Called when an un-handleable error occurs
// After error has been logged, the process exits
void on_fatal_error(enum ErrorType type, char * msg, PCPTR line_num)
{
	// Log the error
	if(type == etPARSE_ERROR) {
		printf("Error at object code line: %llu", line_num);
	}
	printf(msg);

	// Exit the process after the user enters a key
	getchar();
	exit(EXIT_FAILURE);
}
