#include "runtime.h"
#include "core/error.h"
#include "std/coblox_std_lib.h"

struct Runtime {
	Stack * stack;					// The local variable stack (split into block stack frame)
	CallStack * call_stack;			// The call stack (i.e. a stack of return addresses)
	Heap * heap;					// The memory heap (stores persistent block memory)
	PCPTR prog_counter;				// The index into instructions array of the next instruction to execute
	Instruction * instructions;		// The array of instructions, making up the current program
	struct BlockRegistry * block_registry;	// The block registry object created by the parser
};



static inline void exec_std_call_inst(Runtime * r, Instruction * in);
static inline void exec_std_call_store_v_inst(Runtime * r, Instruction * in);



// Allocate a new runtime object
Runtime * alloc_runtime(struct BlockRegistry * block_registry)
{
	if(block_registry == NULL) {
		return NULL;
	}
	Runtime * r = malloc(sizeof *r);
	if(r) {
		r->block_registry = block_registry;
		r->prog_counter = 0;

		// If stack fails to be allocated, return NULL
		r->stack = alloc_stack();
		if(!r->stack) {
			free(r);
			return NULL;
		}

		// If call stack fails to be allocated, return NULL
		r->call_stack = alloc_call_stack();
		if(!r->call_stack) {
			free_stack(r->stack);
			free(r);
			return NULL;
		}

		// If the heap fails to be allocated, return NULL
		r->heap = alloc_heap();
		if(!r->heap) {
			free_stack(r->stack);
			free_call_stack(r->call_stack);
			free(r);
			return NULL;
		}
	}
	return r;
}

// Free a runtime object
void free_runtime(Runtime * r)
{
	if(r) {
		free_stack(r->stack);
		free_call_stack(r->call_stack);
		free_heap(r->heap);
	}
	free(r);
}

// Execute a program (i.e. an array of instructions)
void exec_program(Runtime * r, Instruction * ins, PCPTR num_ins)
{
	// Copy locals to structure
	r->prog_counter = 0;
	r->instructions = ins;

	// Run instructions until the program counter reaches the end
	while(r->prog_counter < num_ins) {
		// Run the next instruction
		exec_instruction(r, ins + r->prog_counter);
	}
}

// Execute an instruction
void exec_instruction(Runtime * r, Instruction * in)
{
	switch(in->type)
	{
	case itERROR:
	{
		// Oh no!!!
		on_fatal_error(etINVALID_INSTRUCTION, "Error: Object code contains invalid instruction", 0);
		break;
	}
	case itBLOCK:
	{
		// Jump over the block (i.e. jump to the block exit line) since it has not been called
		///printf("Block %d enter line: %d\n", in->BLOCK_ID, r->block_registry->block_addresses[in->BLOCK_ID].block_enter_line);
		// NOTE - Block exit line is set to the END instructions index, therefore, when jumped to the +1 (at the end of this function) sets to the line after
		// This is the correct behaviour since in the case of jumping over the block since the END instruction shouldn't be processed
		r->prog_counter = r->block_registry->block_addresses[in->BLOCK_ID].block_exit_line;
		break;
	}
	case itALLOC_1:
	{
		// Create a new stack frame for the block
		reserve_stack_frame(r->stack, in->ALLOC_1_STACK_FRAME_SIZE);
		break;
	}
	case itALLOC_2:
	{
		// Get the return value size
		VALSIZE return_val_size = *(VALSIZE*)peek_stack(r->stack, 0, sizeof return_val_size);
		// If the caller does not store the return value, there is a memory leak
		if(return_val_size == 0) {
			// TODO - In debug mode, store the line in instruction and output it at every fatal error
			on_fatal_error(etAPPLICATION_MEMORY_ERROR, "Error: Memory leak detected in object code", 0);
		}
		// Create a new heap object for the block
		PTR obj_ptr = alloc_object_on_heap_zeroised(r->heap, in->ALLOC_2_HEAP_OBJECT_SIZE);
		// Get the offset into the current stack frame at which to store the return value
		PTR return_val_offset = *(PTR*)peek_stack(r->stack, sizeof return_val_size, sizeof return_val_offset);
		store_in_stack_frame(r->stack, return_val_offset, &obj_ptr, sizeof obj_ptr);
		// Create a new stack frame for the block
		reserve_stack_frame(r->stack, in->ALLOC_2_STACK_FRAME_SIZE);
		// Push the pointer to the object pointer to the stack (allows stack frame to have quick access to the value without having to access return value)
		push_to_stack(r->stack, &obj_ptr, sizeof obj_ptr);
		break;
	}
	case itEND_1:
	{
		// Free the stack frame since the block is exiting
		free_stack_frame(r->stack);
		///printf("Block %d exit line: %d\n", in->BLOCK_ID, r->block_registry->block_addresses[in->BLOCK_ID].block_exit_line);
		// Pop the return value size from the stack
		VALSIZE return_val_size = *(VALSIZE*)peek_stack(r->stack, 0, sizeof return_val_size);
		pop_from_stack(r->stack, sizeof return_val_size);
		// Pop the return value offset from the stack if the size is greater than 0
		if(return_val_size > 0) {
			pop_from_stack(r->stack, return_val_size);
		}
		// Jump to the return pointer, i.e. the instruction which called into the current block
		r->prog_counter = peek_call_stack(r->call_stack);
		pop_from_call_stack(r->call_stack);
		break;
	}
	case itEND_2:
	{
		// Pop the pointer to the heap object for the block from the stack
		PTR obj_ptr = *(PTR*)peek_stack(r->stack, 0, sizeof obj_ptr);
		pop_from_stack(r->stack, sizeof obj_ptr);
		// Free the stack frame since the block is exiting
		free_stack_frame(r->stack);
		///printf("Block %d exit line: %d\n", in->BLOCK_ID, r->block_registry->block_addresses[in->BLOCK_ID].block_exit_line);
		// Pop the return value size from the stack
		VALSIZE return_val_size = *(VALSIZE*)peek_stack(r->stack, 0, sizeof return_val_size);
		pop_from_stack(r->stack, sizeof return_val_size);
		// Pop the return value offset from the stack if the size is greater than 0
		if(return_val_size > 0) {
			pop_from_stack(r->stack, return_val_size);
		}
		// Jump to the return pointer, i.e. the instruction which called into the current block
		r->prog_counter = peek_call_stack(r->call_stack);
		pop_from_call_stack(r->call_stack);
		break;
	}
	case itBSTORE_I:
	{
		// Store the 32-bit integer in the block object
		PTR obj_ptr = *(PTR*)peek_stack(r->stack, 0, sizeof obj_ptr);
		store_in_heap_object(r->heap, obj_ptr, in->BSTORE_I_OFFSET, &in->BSTORE_I_VALUE, sizeof in->BSTORE_I_VALUE);
		break;
	}
	case itFREE:
	{
		// Get the block id from the current stack frame
		PTR obj_ptr = *(PTR*)get_from_stack_frame(r->stack, in->FREE_STACK_FRAME_BLOCK_ID_OFFSET);
		// Free the heap object
		free_object_on_heap(r->heap, obj_ptr);
		break;
	}
	case itBRANCH:
	{
		// Branch by the specified amount
		r->prog_counter += in->BRANCH_I_JUMP_VALUE;
		break;
	}
	case itBRANCH_I_IF_Z:
	{
		// Get the value to test
		int32_t i = *(int32_t*)get_from_stack_frame(r->stack, in->BRANCH_I_VALUE_OFFSET);
		// If the value is zero, get the branch offset and set the program counter
		if(i == 0) {
			r->prog_counter += in->BRANCH_I_JUMP_VALUE;
		}
		break;
	}
	case itBRANCH_I_IF_ZP:
	{
		// Get the value to test
		int32_t i = *(int32_t*)get_from_stack_frame(r->stack, in->BRANCH_I_VALUE_OFFSET);
		// If the value is greater than or equal to zero, get the branch offset and set the program counter
		if(i >= 0) {
			r->prog_counter += in->BRANCH_I_JUMP_VALUE;
		}
		break;
	}
	case itCALL:
	{
		// NOTE - Block enter line is set to the BLOCK instructions index, therefore, when jumped to the +1 (at the end of this function) sets to the line after
		// This is the correct behaviour since in the case of of jumping to the block since the BLOCK instruction shouldn't be processed
		push_to_call_stack(r->call_stack, r->prog_counter);
		r->prog_counter = r->block_registry->block_addresses[in->CALL_BLOCK_ID].block_enter_line;
		// Push a zeroised byte to the stack, this is required as the callee needs to know the return value is not used
		VALSIZE return_val_size = 0;
		push_to_stack(r->stack, &return_val_size, sizeof return_val_size);
		break;
	}
	case itCALL_STORE_V:
	{
		// NOTE - Block enter line is set to the BLOCK instructions index, therefore, when jumped to the +1 (at the end of this function) sets to the line after
		// This is the correct behaviour since in the case of of jumping to the block since the BLOCK instruction shouldn't be processed
		push_to_call_stack(r->call_stack, r->prog_counter);
		r->prog_counter = r->block_registry->block_addresses[in->CALL_BLOCK_ID].block_enter_line;
		// Push the offset into the current stack frame at which the return value is to be stored
		push_to_stack(r->stack, &in->CALL_STORE_V_OFFSET, sizeof in->CALL_STORE_V_OFFSET);
		// Push the return value size to the stack
		// The return value consists of 1 byte giving the size of the data and is followed by the data itself
		VALSIZE return_val_size = sizeof(PTR);
		push_to_stack(r->stack, &return_val_size, sizeof return_val_size);
		break;
	}
	case itCALL_STD:
	{
		// Call a standard library function
		exec_std_call_inst(r, in);
		break;
	}
	case itCALL_STD_STORE_V:
	{
		// Call a standard library function
		exec_std_call_store_v_inst(r, in);
		break;
	}
	case itRETURN:
	{
		// Free the stack frame since the block is exiting
		free_stack_frame(r->stack);
		///printf("Block %d exit line: %d\n", in->BLOCK_ID, r->block_registry->block_addresses[in->BLOCK_ID].block_exit_line);
		// Get the return value size from the stack
		VALSIZE return_val_size = *(VALSIZE*)peek_stack(r->stack, 0, sizeof return_val_size);
		// If the caller expects a return value there is an error since itRETURN does not return a value
		if(return_val_size > 0) {
			///pop_from_stack(r->stack, return_val_size);
			on_fatal_error(etAPPLICATION_MEMORY_ERROR, "Error: Caller expects return value but block returns void", 0);
		}
		// Pop the return value size from the stack
		pop_from_stack(r->stack, sizeof return_val_size);
		// Jump to the return pointer, i.e. the instruction which called into the current block
		r->prog_counter = peek_call_stack(r->call_stack);
		pop_from_call_stack(r->call_stack);
		//
		///PTR return_val_offset = *(PTR*)peek_stack(r->stack, sizeof return_val_size, sizeof return_val_offset);
		///store_in_stack_frame(r->stack, return_val_offset, &obj_ptr, sizeof obj_ptr);
		break;
	}
	case itRETURN_I:
	{
		// Get the return value from the current stack frame
		int32_t i = *(int32_t*)get_from_stack_frame(r->stack, in->RETURN_I_OFFSET);
		// Free the stack frame since the block is exiting
		free_stack_frame(r->stack);
		///printf("Block %d exit line: %d\n", in->BLOCK_ID, r->block_registry->block_addresses[in->BLOCK_ID].block_exit_line);
		// Get the return value size from the stack
		VALSIZE return_val_size = *(VALSIZE*)peek_stack(r->stack, 0, sizeof return_val_size);
		// If the caller doesn't expect a return value, that's ok, it will be ignored
		if(return_val_size > 0) {
			// Pop the return value size from the stack
			pop_from_stack(r->stack, return_val_size);
			// Get the offset at which to store the return value
			PTR return_val_offset = *(PTR*)peek_stack(r->stack, 0, sizeof return_val_offset);
			store_in_stack_frame(r->stack, return_val_offset, &i, sizeof i);
		}
		// Jump to the return pointer, i.e. the instruction which called into the current block
		r->prog_counter = peek_call_stack(r->call_stack);
		pop_from_call_stack(r->call_stack);
		break;
	}
	case itSTORE_I:
	{
		// Store the 32-bit integer in the current stack frame
		store_in_stack_frame(r->stack, in->STORE_I_OFFSET, &in->STORE_I_VALUE, sizeof in->STORE_I_VALUE);
		break;
	}
	case itSTORE_S:
	{
		// Add the string to the heap and store the pointer in the stack frame
		PTR obj_ptr = alloc_object_on_heap(r->heap, in->STORE_S_STRING, in->STORE_S_STRING_SIZE);
		store_in_stack_frame(r->stack, in->STORE_S_OFFSET, &obj_ptr, sizeof obj_ptr);
		break;
	}
	case itLOAD_I:
	{
		// Get the block id from the current stack frame
		PTR obj_ptr = *(PTR*)get_from_stack_frame(r->stack, in->LOAD_I_STACK_FRAME_BLOCK_ID_OFFSET);
		// Get the 32-bit integer from the block's storage
		int32_t i = *(int32_t*)get_from_heap_object(r->heap, obj_ptr, in->LOAD_I_BLOCK_OFFSET);
		// Store the 32-bit integer in the current stack frame
		store_in_stack_frame(r->stack, in->LOAD_I_STACK_FRAME_OFFSET, &i, sizeof i);
		break;
	}
	case itAND_I:
	{
		// Get the integers from the stack frame and and them together
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->AND_I_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->AND_I_OFFSET_2);
		int32_t i3 = i1 & i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->AND_I_OFFSET_3, &i3, sizeof i3);
		break;
	}
	case itOR_I:
	{
		// Get the integers from the stack frame and or them together
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->OR_I_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->OR_I_OFFSET_2);
		int32_t i3 = i1 | i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->OR_I_OFFSET_3, &i3, sizeof i3);
		break;
	}
	case itXOR_I:
	{
		// Get the integers from the stack frame and xor them together
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->XOR_I_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->XOR_I_OFFSET_2);
		int32_t i3 = i1 ^ i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->XOR_I_OFFSET_3, &i3, sizeof i3);
		break;
	}
	case itNOT_I:
	{
		// Get the integer from the stack frame and not it
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->NOT_I_OFFSET_1);
		int32_t i2 = ~i1;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->NOT_I_OFFSET_2, &i2, sizeof i2);
		break;
	}
	case itBNOT_I:
	{
		// Get the integer from the stack frame and boolean not it
		// Boolean not is only defined for input values of 1 and 0
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->BNOT_I_OFFSET_1);
		int32_t i2 = i1 ^ 1;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->BNOT_I_OFFSET_2, &i2, sizeof i2);
		break;
	}
	case itADD_I_2:
	{
		// Get the integers from the stack frame and add them together
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->ADD_I_2_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->ADD_I_2_OFFSET_2);
		int32_t i3 = i1 + i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->ADD_I_2_OFFSET_2, &i3, sizeof i3);
		break;
	}
	case itADD_I_3:
	{
		// Get the integers from the stack frame and add them together
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->ADD_I_3_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->ADD_I_3_OFFSET_2);
		int32_t i3 = i1 + i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->ADD_I_3_OFFSET_3, &i3, sizeof i3);
		break;
	}
	case itSUB_I_2:
	{
		// Get the integers from the stack frame and subtract the 2nd from the 1st
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->SUB_I_2_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->SUB_I_2_OFFSET_2);
		int32_t i3 = i1 - i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->ADD_I_2_OFFSET_2, &i3, sizeof i3);

		break;
	}
	case itSUB_I_3:
	{
		// Get the integers from the stack frame and subtract the 2nd from the 1st
		int32_t i1 = *(int32_t*)get_from_stack_frame(r->stack, in->SUB_I_3_OFFSET_1);
		int32_t i2 = *(int32_t*)get_from_stack_frame(r->stack, in->SUB_I_3_OFFSET_2);
		int32_t i3 = i1 - i2;
		// Store the new value in the stack frame
		store_in_stack_frame(r->stack, in->SUB_I_3_OFFSET_3, &i3, sizeof i3);
		break;
	}
	case itSTORE_GET_I:
	{
		// Pause execution until a 32-bit integer is entered into the terminal
		// TODO - Replace with a stream interface using fgets
		int32_t i;
		fflush(stdin);
		scanf("%d", &i);
		store_in_stack_frame(r->stack, in->STORE_GET_I_OFFSET, &i, sizeof i);
		break;
	}
	case itPRINT_I:
	{
		// Interpret the stack data at offset as a 32-bit integer and print it
		int32_t i = *(int32_t*)get_from_stack_frame(r->stack, in->PRINT_I_OFFSET);
		printf("%d\n", i);
		break;
	}
	case itPRINT_S:
	{
		// Interpret the stack data as an offset to a pointer to a string and print it
		PTR obj_ptr = *(PTR*)get_from_stack_frame(r->stack, in->PRINT_S_OFFSET);
		printf("%s\n", get_from_heap_object(r->heap, obj_ptr, 0));
		break;
	}
	default:
		break;
	}

	// NOTE - Block enter and exits lines are set to the BLOCK and END instructions indexes, therefore, when jumped to the +1 sets to the line after
	// This is the correct behaviour since in the case of calling and the case of jumping over the block since those instructions shouldn't be processed
	r->prog_counter ++;
}

static inline void exec_std_call_inst(Runtime* r, Instruction* in)
{
	switch (in->CALL_STD_FUNC_ID)
	{
	case 0:
		std_global_print("Standard Library Call");
		break;
	case 1:
		std_global_println("Standard Library Call");
		break;
	case 2:
		std_global_printf("Standard Library Call");
		break;
	}
}

static inline void exec_std_call_store_v_inst(Runtime* r, Instruction* in)
{
	switch (in->CALL_STD_FUNC_ID)
	{
	case 5:
	{
		// TODO - REMOVE TEST
		int i = test_get_1();
		store_in_stack_frame(r->stack, in->CALL_STD_STORE_V_OFFSET, &i, sizeof i);
		break;
	}
	}
}
