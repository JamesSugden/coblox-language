#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "core/types.h"

typedef struct Heap Heap;

// Allocate a new heap object
Heap * alloc_heap();

// Free a heap object
void free_heap(Heap * h);

// Create memory for a new object
// Zeroises the object data
// Returns index into the heap objects array of the object
PTR alloc_object_on_heap_zeroised(Heap * h, SPTR data_size);

// Create memory for a new object
// Sets the initial data of the object by taking ownership of the data passed
// Returns index into the heap objects array of the object
PTR alloc_object_on_heap(Heap * h, char * data, SPTR data_size);

// Free memory of an object
void free_object_on_heap(Heap * h, PTR obj_ptr);

// Store data in a heap object
void store_in_heap_object(Heap * h, PTR obj_ptr, SPTR offset, char * data, SPTR data_size);

// Get data from heap object
char * get_from_heap_object(Heap * h, PTR obj_ptr, SPTR offset);
