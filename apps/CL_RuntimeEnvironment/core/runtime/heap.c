#include "heap.h"
#include "core/error.h"

#define DEFAULT_OBJECTS_SIZE 65535//512

typedef struct {
	// TODO - Consider replacing individual malloc'd memory blocks with pre-allocated blocks of fixed size
	char * data;
	SPTR data_size;
} Object;

struct Heap {
	// TODO - Consider replacing individual malloc'd memory blocks for each object to one big malloc'd block which all objects share
	Object * objects;		// Array of object data blocks, pointer to a block is an index into this array
	PTR objects_size;		// The length of the objects array (not necessarily the number of objects)
};



// Resize the heap to double its size
static inline void resize_heap(Heap * h);



// Allocate a new heap object
Heap * alloc_heap()
{
	Heap * h = malloc(sizeof *h);
	if(h) {
		h->objects_size = DEFAULT_OBJECTS_SIZE;
		h->objects = calloc(h->objects_size, sizeof *h->objects);

		if(!h->objects) {
			free(h);
			return NULL;
		}

		for(PTR i = 0; i < h->objects_size; ++i) {
			h->objects[i].data = NULL;
		}
	}
	return h;
}

// Free a heap object
void free_heap(Heap * h)
{
	if(h) {
		// First, free the data of each heap object
		for(PTR i = 0; i < h->objects_size; ++i) {
			free(h->objects[i].data);
		}
		// Then, free the array of objects
		free(h->objects);
	}
	free(h);
}

// Create memory for a new object
// Zeroises the object data
PTR alloc_object_on_heap_zeroised(Heap * h, SPTR data_size)
{
	// Find an unused object
	for(int i = 0; i < h->objects_size; i++) {
		if(h->objects[i].data == NULL) {
			h->objects[i].data_size = data_size;
			h->objects[i].data = calloc(data_size, sizeof(char));
			// TODO - Handle calloc error
			return i;
		}
	}
	// If an unused object could not be found, realloc the data array
	uint64_t ptr = h->objects_size;
	resize_heap(h);
	// Set the next available object
	h->objects[ptr].data_size = data_size;
	h->objects[ptr].data = calloc(data_size, sizeof(char));
	// TODO - Handle calloc error
	return ptr;
}

// Create memory for a new object
// Sets the initial data of the object by taking ownership of the data passed
PTR alloc_object_on_heap(Heap * h, char * data, SPTR data_size)
{
	// Find an unused object
	for(PTR i = 0; i < h->objects_size; i++) {
		if(h->objects[i].data == NULL) {
			h->objects[i].data_size = data_size;
			h->objects[i].data = data;
			// TODO - Handle calloc error
			return i;
		}
	}
	// If an unused object could not be found, realloc the data array
	uint64_t ptr = h->objects_size;
	resize_heap(h);
	// Set the next available object
	h->objects[ptr].data_size = data_size;
	h->objects[ptr].data = data;
	// TODO - Handle calloc error
	return ptr;
}

// Free memory of an object
void free_object_on_heap(Heap * h, PTR obj_ptr)
{
	free(h->objects[obj_ptr].data);
	h->objects[obj_ptr].data = NULL;
}

// Store data in a heap object
void store_in_heap_object(Heap * h, PTR obj_ptr, SPTR offset, char * data, SPTR data_size)
{
	memcpy(h->objects[obj_ptr].data + offset, data, data_size);
}

// Get data from heap object
char * get_from_heap_object(Heap * h, PTR obj_ptr, SPTR offset)
{
	return h->objects[obj_ptr].data + offset;
}

// Resize the heap to double its size
static inline void resize_heap(Heap * h)
{
	PTR new_size = h->objects_size * 2;
	Object * new_objects = realloc(h->objects, new_size);
	// If the realloc failed, exit the program with an error
	if(new_objects == NULL) {
		on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to re-allocate the heap", 0);
	} else  {
		// Zeroises the new memory
		for(PTR i = h->objects_size; i < new_size; ++i) {
			new_objects[i].data = NULL;
		}
		h->objects = new_objects;
		h->objects_size = new_size;
	}
}
