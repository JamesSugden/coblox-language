#include "stack.h"
#include "core/error.h"

#define DEFAULT_STACK_SIZE 1048576

struct Stack {
	char * data;
	uint64_t stack_size;
	uint64_t stack_ptr;
	uint64_t block_ptr;		// Points to the start of a block's stack frame (also known as the stack frame pointer)
};



// Resize the stack to double its size
static inline void resize_stack(Stack * s, size_t required_size);



// Allocate a new stack object
Stack * alloc_stack()
{
	Stack * s = malloc(sizeof *s);
	if(s) {
		s->stack_ptr = 0;
		s->block_ptr = 0;
		s->stack_size = DEFAULT_STACK_SIZE;
		s->data = calloc(s->stack_size, sizeof *s->data);

		if(!s->data) {
			free(s);
			return NULL;
		}
	}
	return s;
}

// Free a stack object
void free_stack(Stack * s)
{
	if(s) {
		free(s->data);
	}
	free(s);
}

// Reserve a section of the stack for the local storage of a code block
void reserve_stack_frame(Stack * s, PTR data_size)
{
	// If the stack is too small, realloc it
	// Add the sizeof(PTR) incase the block needs to push its heap object pointer
	size_t required_size = s->stack_ptr + data_size + (sizeof s->block_ptr) + sizeof(PTR);
	if(required_size > s->stack_size) {
		resize_stack(s, required_size);
	}

	// Push the current block pointer to the stack
	push_to_stack(s, &s->block_ptr, sizeof s->block_ptr);

	// Set the start of the new block's storage to the stack pointer
	s->block_ptr = s->stack_ptr;

	// Increment the stack pointer to point to the end
	s->stack_ptr += data_size;
}

// Free previously reserved storage
void free_stack_frame(Stack * s)
{
	// Pop the current block's storage from the stack
	pop_from_stack(s, s->stack_ptr - s->block_ptr);

	// Set the block pointer back to the previous block pointer
	s->block_ptr = *peek_stack(s, 0, sizeof s->block_ptr);

	// Pop the current block pointer from the stack since it is now stored in s->block_ptr
	pop_from_stack(s, sizeof s->block_ptr);
}

// Push arbitrary data to the stack
void push_to_stack(Stack * s, char * data, PTR data_size)
{
	// Push the data to the stack
	// Copy into the stack data at the stack pointer
	memcpy(s->data + s->stack_ptr, data, data_size);
	s->stack_ptr += data_size;
}

// Pop arbitrary data from the stack
void pop_from_stack(Stack * s, PTR data_size)
{
	// If there is not enough data to pop, there is an error
	if(s->stack_ptr >= data_size) {
		memset(s->data + s->stack_ptr, 0, data_size);
		s->stack_ptr -= data_size;
	} else {
		on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to pop from the local data stack, pop out of range", 0);
	}
}

// Peek the top of the stack
char * peek_stack(Stack * s, PTR offset, PTR peek_size)
{
	// If there is not enough data to peek at, there is an error
	if(s->stack_ptr >= peek_size)
	{
		return s->data + s->stack_ptr - peek_size - offset;
	}
	on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to peek the local data stack, peek out of range", 0);
	return NULL;
}

// Store data in the current stack frame
void store_in_stack_frame(Stack * s, PTR offset, char * data, PTR data_size)
{
	memcpy(s->data + s->block_ptr + offset, data, data_size);
}

// Get data from current stack frame
char * get_from_stack_frame(Stack * s, PTR offset)
{
	return s->data + s->block_ptr + offset;
}

// Resize the stack to double its size
static inline void resize_stack(Stack * s, size_t required_size)
{
	uint64_t new_size = s->stack_size;
	while(new_size < required_size) {
		new_size *= 2;
	}
	char * new_data = realloc(s->data, new_size);
	// If the realloc failed, exit the program with an error
	if(new_data == NULL) {
		on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to re-allocate the local data stack", 0);
	} else {
		s->data = new_data;
		s->stack_size = new_size;
	}
}
