#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "core/types.h"

typedef struct CallStack CallStack;

// Allocate and initialise a new call stack object
CallStack * alloc_call_stack();

// Free a call stack object
void free_call_stack(CallStack * s);

// Push a return address to the call stack
void push_to_call_stack(CallStack * s, PCPTR return_address);

// Pop a return address from the call stack
void pop_from_call_stack(CallStack * s);

// Peek the top return address of the call stack
PCPTR peek_call_stack(CallStack * s);
