#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "core/parser/instruction.h"
#include "core/parser/block_registry.h"
#include "stack.h"
#include "call_stack.h"
#include "heap.h"

typedef struct Runtime Runtime;

// Allocate a new runtime object
Runtime * alloc_runtime(struct BlockRegistry * block_registry);

// Free a runtime object
void free_runtime(Runtime * r);

// Execute a program (i.e. an array of instructions)
void exec_program(Runtime * r, Instruction * ins, PCPTR num_ins);

// Execute an instruction
// Should only called outside of runtime.c if program is being run in a debugger
void exec_instruction(Runtime * r, Instruction * in);
