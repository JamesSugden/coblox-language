#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "core/types.h"

typedef struct Stack Stack;

// Allocate a new stack object
Stack * alloc_stack();

// Free a stack object
void free_stack(Stack * s);

// Reserve a section of the stack for the local storage of a code block
void reserve_stack_frame(Stack * s, PTR data_size);

// Free previously reserved storage
void free_stack_frame(Stack * s);

// Store data in the current stack frame
void store_in_stack_frame(Stack * s, PTR offset, char * data, PTR data_size);

// Get data from current stack frame
char * get_from_stack_frame(Stack * s, PTR offset);

// Push arbitrary data to the stack
void push_to_stack(Stack * s, char * data, PTR data_size);

// Pop arbitrary data from the stack
void pop_from_stack(Stack * s, PTR data_size);

// Peek the top of the stack
char * peek_stack(Stack * s, PTR offset, PTR peek_size);
