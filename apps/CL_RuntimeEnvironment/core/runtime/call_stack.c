#pragma once
#include "call_stack.h"
#include "core/error.h"

#define DEFAULT_CALL_STACK_SIZE 64
#define MAX_CALL_DEPTH 1024

struct CallStack {
	PCPTR * return_addresses;		// TODO - Consider replacing with call stack which can grow (up to a max length)
	uint64_t stack_ptr;				// Index of the current address
	uint64_t stack_size;			// The total length of the return_addresses array
};



// Resize the return addresses array of the call stack
static inline void resize_call_stack(CallStack * s);



// Allocate a new stack object
CallStack * alloc_call_stack()
{
	CallStack * s = malloc(sizeof *s);
	if(s) {
		s->stack_ptr = 0;
		s->stack_size = DEFAULT_CALL_STACK_SIZE;
		s->return_addresses = calloc(s->stack_size, sizeof *s->return_addresses);

		if(!s->return_addresses) {
			free(s);
			return NULL;
		}
	}
	return s;
}

// Free a stack object
void free_call_stack(CallStack * s)
{
	if(s) {
		free(s->return_addresses);
	}
	free(s);
}

// Push a return address to the call stack
void push_to_call_stack(CallStack * s, PCPTR return_address)
{
	// If the maximum call stack size has been reached, attempt to resize the call stack
	if(s->stack_ptr == s->stack_size-1) {
		resize_call_stack(s);
	}

	// Push the return address to the call stack
	s->return_addresses[s->stack_ptr] = return_address;

	// Increment the stack pointer
	s->stack_ptr ++;
}

// Pop a return address from the call stack
void pop_from_call_stack(CallStack * s)
{
	// Decrement the stack pointer
	s->stack_ptr --;
}

// Peek the top return address of the call stack
PCPTR peek_call_stack(CallStack * s)
{
	if(s->stack_ptr > 0) {
		return s->return_addresses[s->stack_ptr-1];
	}
	// If the call stack has nothing in it, exit the program with an error
	on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to peek the call stack, call stack is empty", 0);
	return 0;
}

// Resize the return addresses array of the call stack
static inline void resize_call_stack(CallStack * s)
{
	uint64_t new_size = s->stack_size + DEFAULT_CALL_STACK_SIZE;
	if(new_size > MAX_CALL_DEPTH) {
		// Call stack has exceeded the maximum call depth (probably because of a recursion error)
		// Therefore, throw a fatal error
		on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Maximum recursion depth reached, call stack out of memory", 0);
	}
	PCPTR * new_return_addresses = realloc(s->return_addresses, new_size);
	// If the realloc fails, exit the program with an error
	if(new_return_addresses == NULL) {
		on_fatal_error(etINTERNAL_MEMORY_ERROR, "Error: Failed to re-allocate the call stack", 0);
	} else {
		s->return_addresses = new_return_addresses;
		s->stack_size = new_size;
	}
}
