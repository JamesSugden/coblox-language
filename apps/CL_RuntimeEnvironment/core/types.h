#pragma once
#include <stdint.h>

// ID of a code block
typedef uint64_t ID;

// PTR into memory (e.g. used for offsets into stack memory)
typedef uint64_t PTR;

// Short PTR into memory (e.g. used for offsets into block object memory)
typedef uint64_t SPTR;

// Size of a value type (i.e. stores the number of bytes of a value type)
typedef uint8_t VALSIZE;

// PTR into the instruction array (i.e. the line number of an instruction)
typedef uint64_t PCPTR;

// Short offset into the instruction array (e.g. used for relative branches)
typedef int16_t SPCOFFSET;
