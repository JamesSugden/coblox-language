#include "stdafx.h"
#include "core/Compiler.h"
#include "core/file_system/File.h"

using namespace compiler;

int main()
{
	std::string file_path((std::filesystem::path(CL_ROOT_PROJECT_DIRECTORY) / "examples" / "source_code" / "example1.cblx").u8string());
	std::unique_ptr<File> file = File::LoadFile(file_path);

	if(file)
	{
		Compiler c;
		c.CompileProgram(*file);
	}
	else
	{
		std::cerr << "Failed to load file: " << file_path << "\n";
	}

	printf("\n"
		"----------------------------------------------------------------------------------\n"
		"-- Execution finished, press any key to continue\n"
		"----------------------------------------------------------------------------------");
	(void)getchar();
	return 0;
}
