#include "stdafx.h"
#include "SourceFileRegistry.h"
#include "core/file_system/File.h"

namespace compiler
{
	SourceFileRegistry::SourceFileRegistry()
	{

	}

	SourceFileRegistry::~SourceFileRegistry()
	{

	}

	// Registers a file as loaded iff it has not already been loaded
	// Searches in the caller file's directory followed by the library paths in order
	// Returns true if the file is added, returns false if the file has already been loaded
	bool SourceFileRegistry::RegisterSourceFile(std::string const & path)
	{
		// TODO - Register the source file path

		// First, check for the files existence in the cwd
		if(File::Exists(path))
		{
			std::cout << "Source file exists: " << path << "\n";
		}
		else
		{
			// Check for the files existence in the library paths
			for(std::string const & lib_path : library_paths_)
			{
				std::string p = lib_path + "/" + path;
				if(File::Exists(p))
				{
					std::cout << "Source file exists: " << p << "\n";
					break;
				}
			}
		}
		return false;
	}
}
