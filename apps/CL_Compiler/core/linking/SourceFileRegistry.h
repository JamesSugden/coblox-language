#pragma once

namespace compiler
{
	class SourceFileRegistry
	{
	public:
		SourceFileRegistry();
		~SourceFileRegistry();

		// Registers a file as loaded iff it has not already been loaded
		// Searches in the caller file's directory followed by the library paths in order
		// Returns true if the file is added, returns false if the file has already been loaded
		bool RegisterSourceFile(std::string const & path);

	private:
		// List of library paths (paths import statements are relative to)
		std::vector<std::string> library_paths_;
	};
}
