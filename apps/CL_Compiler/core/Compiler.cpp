#include "stdafx.h"
#include "Compiler.h"
#include "syntax_parsing/parse_tree/AbstractSyntaxTree.h"

namespace compiler
{
	Compiler::Compiler()
	{

	}

	Compiler::~Compiler()
	{

	}

	// Compile a Coblox program
	// File passed is the file containing the entry point to the program
	void Compiler::CompileProgram(File const & f)
	{
		// Get a stream of tokens from the source code
		std::unique_ptr<TokenStream> token_stream { lexer_.GetTokenStream(f.GetContents()) };

		if(!token_stream)
			return;

		// Parse the token stream and generate an abstract syntax tree
		auto ast { parser_.ParseTokenStream(token_stream.get()) };

		if(!ast)
			return;

		// Analyse the abstract syntax tree
		//analyser_.AnalyseAst(ast.get());
	}
}
