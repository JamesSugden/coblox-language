#pragma once

namespace compiler
{
	class File
	{
	public:
		virtual ~File();

		std::string const & GetPath() const { return path_; }
		std::string const & GetContents() const { return contents_; }

		static std::unique_ptr<File> LoadFile(std::string const & path);
		static bool SaveFile(std::string const & path, std::string const & contents);

		static bool Exists(std::string const & path);

	private:
		File(std::string const & path, std::string const & contents);

		std::string path_;
		std::string contents_;
	};
}
