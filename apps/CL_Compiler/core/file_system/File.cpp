#include "stdafx.h"
#include "File.h"

namespace compiler
{
	File::File(std::string const & path, std::string const & contents) : path_(path), contents_(contents)
	{

	}

	File::~File()
	{

	}

	std::unique_ptr<File> File::LoadFile(std::string const & path)
	{
		std::ifstream in(path, std::ios::in);
		if(in)
		{
			std::string contents;
			in.seekg(0, std::ios::end);
			contents.resize(in.tellg());
			in.seekg(0, std::ios::beg);
			in.read(&contents[0], contents.size());
			in.close();
			return std::unique_ptr<File>(new File(path, contents));
		}
		return nullptr;
	}

	bool File::SaveFile(std::string const & path, std::string const & contents)
	{
		std::ofstream out(path, std::ios::out);
		if(out)
		{
			out << contents;
			out.close();
			return true;
		}
		return false;
	}

	bool File::Exists(std::string const & path)
	{
		return std::filesystem::exists(path);
	}
}
