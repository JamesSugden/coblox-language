#include "stdafx.h"
#include "SymbolTable.h"
#include "Scope.h"

namespace compiler
{
	SymbolTable::SymbolTable()
	{
		global_scope_ = std::make_unique<Scope>(nullptr);
	}

	SymbolTable::~SymbolTable()
	{

	}
}
