#include "stdafx.h"
#include "Scope.h"
#include "Symbol.h"

namespace compiler
{
	Scope::Scope(Scope * parent) : parent_(parent)
	{

	}

	Scope::~Scope()
	{

	}

	// Add a sub-scope to this scope
	Scope * Scope::AddScope()
	{
		sub_scopes_.push_back(std::make_unique<Scope>(this));
		return sub_scopes_.back().get();
	}

	// Lookup a symbol this scope first, then its parent scopes
	Symbol * Scope::Lookup(std::string const & name)
	{
		Symbol * s = LocalLookup(name);
		if(!s && !parent_)
			return parent_->Lookup(name);
		return s;
	}

	// Add a new symbol to this scope
	Symbol * Scope::AddSymbol(std::string const & name)
	{
		Symbol * s = LocalLookup(name);
		if(!s)
		{
			std::unique_ptr<Symbol> symbol = std::make_unique<Symbol>();
			Symbol * ptr = symbol.get();
			symbols_.emplace(name, std::move(symbol));
			return ptr;
		}
		return nullptr;
	}

	// Lookup a symbol in this specific scope
	Symbol * Scope::LocalLookup(std::string const & name)
	{
		auto iter = symbols_.find(name);
		return iter == symbols_.end() ? nullptr : iter->second.get();
	}
}
