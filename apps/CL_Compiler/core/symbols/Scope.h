#pragma once

namespace compiler
{
	class Symbol;

	class Scope
	{
	public:
		Scope(Scope * parent);
		~Scope();

		// Add a sub-scope to this scope
		Scope * AddScope();

		// Lookup a symbol in this scope first, then its parent scopes
		Symbol * Lookup(std::string const & name);

		// Add a new symbol to this scope
		// Returns the new symbol or nullptr iff the name is already taken
		Symbol * AddSymbol(std::string const & name);

		// Lookup a symbol in this specific scope
		Symbol * LocalLookup(std::string const & name);

	private:
		// The parent scope of this scope
		// Nullptr iff this scope is the global scope
		Scope * parent_;

		// List of direct sub-scopes
		std::vector<std::unique_ptr<Scope>> sub_scopes_;

		// Map from symbol name to symbol object
		std::unordered_map<std::string, std::unique_ptr<Symbol>> symbols_;
	};
}
