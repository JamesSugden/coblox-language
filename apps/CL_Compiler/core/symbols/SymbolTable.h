#pragma once

namespace compiler
{
	class Scope;
	class Symbol;

	class SymbolTable
	{
	public:
		SymbolTable();
		~SymbolTable();

		// Get a pointer to the global scope
		Scope * GetGlobalScope() const { return global_scope_.get(); }

	private:
		std::unique_ptr<Scope> global_scope_;
	};
}
