#include "stdafx.h"
#include "Parser.h"
#include "parse_tree/AbstractSyntaxTree.h"
#include "parse_tree/Node.h"
#include "core/file_system/File.h"

#define GRAMMAR_PRODUCTION_LOGGING 1
#define GRAMMAR_MATCH_LOGGING 1

#if GRAMMAR_PRODUCTION_LOGGING
#	define LOG_GRAMMAR(x) do { std::clog << x << "\n"; } while(0);
#else
#	define LOG_GRAMMAR(x) do {} while(0);
#endif

#if GRAMMAR_MATCH_LOGGING
#	define LOG_MATCH(x) do { std::clog << x << "\n"; } while(0);
#else
#	define LOG_MATCH(x) do {} while(0);
#endif

namespace compiler
{
	Parser::Parser()
	{

	}

	Parser::~Parser()
	{

	}

	// Parse a stream of lexical units (tokens)
	// Constructs a parse tree
	// Throws if syntax is invalid
	std::unique_ptr<AbstractSyntaxTree> Parser::ParseTokenStream(TokenStream * token_stream)
	{
		std::cout << "\n============================";
		std::cout << "\n== Parsing Syntax";
		std::cout << "\n============================\n\n";

		token_stream_ = token_stream;

		auto ast = std::make_unique<AbstractSyntaxTree>();

		// If the stream consists of no tokens, don't try to parse it
		if(token_stream->HasFirstToken())
		{
			try
			{
				Statements(ast->GetRootNode());
				std::clog << "Syntax is valid\n";
			}
			catch(SyntaxInvalidException & e)
			{
				std::cerr << e.what() << "\n";
			}
		}

		auto path = std::filesystem::path(CL_ROOT_PROJECT_DIRECTORY) / "examples" / "output_asts" / "ast.txt";
		if(!File::SaveFile(path.u8string(), ast->ToString()))
		{
			std::cerr << "Failed to write file: " << path.u8string() << "\n";
		}
		return std::move(ast);
	}

	// Parse a global statements production
	void Parser::Statements(NodeListNode & parent)
	{
		LOG_GRAMMAR("Statements");

		// Parse a single statement (which may be surrounded by new lines)
		NewLines();
		auto node = Statement();
		NewLines();

		// If there was a statement, try to parse another or end the list of statements
		if(node)
		{
			parent.children_.push_back(std::move(node));
			if(!IsTokenSource("}"))
				Statements(parent);
		}
	}

	// Parse a global statement production
	std::unique_ptr<Node> Parser::Statement()
	{
		LOG_GRAMMAR("Statement");

		std::unique_ptr<Node> node;
		if(IsTokenSource("{-}") || IsTokenSource("{+}") || IsTokenSource("<->") || IsTokenSource("<+>"))
		{
			// Statements starting with a block/template visibility specifier must declare a field
			node = FieldDeclaration();
		}
		else if(IsTokenType(ETokenType::IDENTIFIER))
		{
			// The statement is either a local declaration or an expression starting with an identifier
			node = LocalDeclarationOrIdentifierExpression();
		}
		else if(IsTokenSource("return"))
		{
			// Return statements specify an expression to be returned from a code block
			node = ReturnStatement();
		}

		// Each statement must be terminated with a newline
		// This will also catch erroneous tokens appearing where statement tokens should be
		if(token_stream_->IsLookaheadValid() && !IsTokenType(ETokenType::NEWLINE))
		{
			throw BuildSyntaxInvalidException("newline");
		}
		return node;
	}

	// Parse zero or more consecutive new lines
	void Parser::NewLines()
	{
		while(IsTokenType(ETokenType::NEWLINE))
		{
			MatchTokenType(ETokenType::NEWLINE);
		}
	}

	// Parse a field declaration production (decleration of a block/template field within a code block)
	std::unique_ptr<Node> Parser::FieldDeclaration()
	{
		LOG_GRAMMAR("Field Declaration");

		// Determine the field keyword
		ENodeType node_type = ENodeType::OBJECT_HIDDEN;
		if(IsTokenSource("{-}"))
		{
			MatchTokenSource("{-}");
			node_type = ENodeType::OBJECT_HIDDEN;
		}
		else if(IsTokenSource("{+}"))
		{
			MatchTokenSource("{+}");
			node_type = ENodeType::OBJECT_VISIBLE;
		}
		else if(IsTokenSource("<->"))
		{
			MatchTokenSource("<->");
			node_type = ENodeType::TEMPLATE_HIDDEN;
		}
		else if(IsTokenSource("<+>"))
		{
			MatchTokenSource("<+>");
			node_type = ENodeType::TEMPLATE_VISIBLE;
		}

		// Match the dot following the field keyword
		MatchTokenSource(".");

		// Create a dot operator node with the field keyword on the left & the rhs identifier on the right
		auto dot_node = std::make_unique<BinaryOpNode>(ENodeType::DOT);
		dot_node->left_ = std::make_unique<Node>(node_type);
		dot_node->right_ = std::make_unique<IdentifierNode>(ENodeType::IDENTIFIER, token_stream_->GetLookahead().source_);

		// Match the rhs identifier
		MatchTokenType(ETokenType::IDENTIFIER);

		// The field declaration can optionally have a trailing type
		// TODO - Support user-created types (identifiers potentially involving the dot operator)
		if(IsTokenType(ETokenType::NATIVE_TYPE) || IsTokenType(ETokenType::IDENTIFIER))
		{
			Type(*dot_node->right_);
		}

		// The statement may contain an assignment
		if(IsTokenSource("=") || IsTokenSource("<-"))
		{
			auto parent_node = Assignment();
			parent_node->left_ = std::move(dot_node);
			return parent_node;
		}
		return dot_node;
	}

	// Parse a local declaration/identifier expression
	std::unique_ptr<Node> Parser::LocalDeclarationOrIdentifierExpression()
	{
		LOG_GRAMMAR("Local Declaration or Identifier Expression");

		// Both a local declaration and expression start with an identifier
		std::unique_ptr<Node> node = std::make_unique<IdentifierNode>(ENodeType::IDENTIFIER, token_stream_->GetLookahead().source_);
		MatchTokenType(ETokenType::IDENTIFIER);

		// If there is a type after the identifier then this is a local declaration
		if(IsTokenType(ETokenType::NATIVE_TYPE) || IsTokenType(ETokenType::IDENTIFIER))
		{
			auto type_node = std::make_unique<IdentifierNode>(ENodeType::IDENTIFIER, "");
			Type(*type_node);
			//node->var_type_ = std::move(type_node);
		}
		else
		{
			// There is no type so this may be a dotted identifier
			node = Dot(std::move(node));

			// If there is a call, exit now to prevent assignment being allowed
			if(IsTokenSource(":"))
			{
				node = Call(std::move(node));
				return node;
			}
		}

		// A local declaration/identifier expression may be followed by an assignment
		if(IsTokenSource("=") || IsTokenSource("<-"))
		{
			auto parent_node = Assignment();
			parent_node->left_ = std::move(node);
			return parent_node;
		}
		return node;
	}

	// Parse an rvalue production
	std::unique_ptr<Node> Parser::RValue()
	{
		LOG_GRAMMAR("RValue");
		if(IsTokenSource("["))
		{
			return ParameterisedCodeBlock();
		}
		else if(IsTokenSource("{"))
		{
			return CodeBlock();
		}
		else if(IsTokenSource("null"))
		{
			auto node = std::make_unique<RValNullLiteralNode>(ENodeType::RVAL_NULL_LITERAL);
			MatchTokenSource("null");
			return node;
		}
		else
		{
			return Expression();
		}
	}

	// Parse a type production
	void Parser::Type(Node & node)
	{
		LOG_GRAMMAR("Type");

		// Parse the type of the node
		if(IsTokenType(ETokenType::NATIVE_TYPE))
		{
			MatchTokenType(ETokenType::NATIVE_TYPE);
		}
		else if(IsTokenType(ETokenType::IDENTIFIER))
		{
			// TODO - Support complex custom identifiers with dots
			MatchTokenType(ETokenType::IDENTIFIER);
		}
		else
		{
			throw BuildSyntaxInvalidException("type");
		}

		// The optional modifier can be used on any type
		if(IsTokenSource("?"))
		{
			MatchTokenSource("?");
		}
	}

	// Parse an assignment production
	std::unique_ptr<BinaryOpNode> Parser::Assignment()
	{
		LOG_GRAMMAR("Assignment");

		// Determine the type of assignment from the operator used
		ENodeType node_type = ENodeType::CONSTANT_ASSIGNMENT;
		if(IsTokenSource("="))
		{
			MatchTokenSource("=");
			node_type = ENodeType::CONSTANT_ASSIGNMENT;
		}
		else
		{
			MatchTokenSource("<-");
			node_type = ENodeType::VARIABLE_ASSIGNMENT;
		}

		// Create the assignment node (the left side will be set by the caller)
		auto node = std::make_unique<BinaryOpNode>(node_type);
		node->right_ = RValue();
		return node;
	}

	// Parse a dot production (some identifier dotted with another, e.g. a.b)
	std::unique_ptr<Node> Parser::Dot(std::unique_ptr<Node> lhs)
	{
		LOG_GRAMMAR("Dot");

		// If no lhs identifier has been provided, match an identifier and create a new node
		std::unique_ptr<Node> node;
		if(lhs)
		{
			node = std::move(lhs);
		}
		else
		{
			node = std::make_unique<IdentifierNode>(ENodeType::IDENTIFIER, token_stream_->GetLookahead().source_);
			MatchTokenType(ETokenType::IDENTIFIER);
		}

		if(IsTokenSource("."))
		{
			// Match the dot operator
			MatchTokenSource(".");

			// Create a dot node
			auto dot_node = std::make_unique<BinaryOpNode>(ENodeType::DOT);
			dot_node->left_ = std::move(node);
			dot_node->right_ = Dot(nullptr);

			return dot_node;
		}
		return node;
	}

	// Parse a call production
	std::unique_ptr<Node> Parser::Call(std::unique_ptr<Node> lhs)
	{
		LOG_GRAMMAR("Call");

		auto dot_node = Dot(std::move(lhs));

		if(IsTokenSource(":"))
		{
			// Match the call operator
			MatchTokenSource(":");

			// Create a call node
			auto call_node = std::make_unique<BinaryOpNode>(ENodeType::CALL);
			call_node->left_ = std::move(dot_node);

			// Parse the arguments to the call
			if(IsTokenSource("["))
			{
				call_node->right_ = ArgumentList();
			}
			else
			{
				call_node->right_ = RValue();
			}
			return call_node;
		}
		return dot_node;
	}

	// Parse an argument list production
	std::unique_ptr<Node> Parser::ArgumentList()
	{
		LOG_GRAMMAR("Argument List");
		auto node = std::make_unique<NodeListNode>(ENodeType::ARGUMENT_LIST);
		MatchTokenSource("[");
		if(!IsTokenSource("]"))
			Arguments(*node);
		MatchTokenSource("]");
		return node;
	}

	// Parse an arguments production
	void Parser::Arguments(NodeListNode & parent)
	{
		LOG_GRAMMAR("Arguments");
		auto node = Expression();
		if(node)
			parent.children_.push_back(std::move(node));
		if(IsTokenSource(","))
		{
			MatchTokenSource(",");
			Arguments(parent);
		}
	}

	// Parse a code block production
	std::unique_ptr<Node> Parser::CodeBlock()
	{
		LOG_GRAMMAR("Code Block");
		auto node = std::make_unique<NodeListNode>(ENodeType::BLOCK);
		MatchTokenSource("{");
		Statements(*node);
		MatchTokenSource("}");
		return node;
	}

	// Parse a code block (with a parameter list) production
	std::unique_ptr<Node> Parser::ParameterisedCodeBlock()
	{
		LOG_GRAMMAR("Parameterised Code Block");
		auto node = std::make_unique<BinaryOpNode>(ENodeType::PARAMETERISED_BLOCK);
		node->left_ = ParameterList();
		node->right_ = CodeBlock();
		return node;
	}

	// Parse a code block parameter list production
	std::unique_ptr<Node> Parser::ParameterList()
	{
		LOG_GRAMMAR("Parameter List");
		auto node = std::make_unique<NodeListNode>(ENodeType::PARAMETER_LIST);
		MatchTokenSource("[");
		if(!IsTokenSource("]"))
			Parameters(*node);
		MatchTokenSource("]");
		return node;
	}

	// Parse a parameters production
	void Parser::Parameters(NodeListNode & parent)
	{
		LOG_GRAMMAR("Parameters");
		auto node = std::make_unique<IdentifierNode>(ENodeType::IDENTIFIER, token_stream_->GetLookahead().source_);
		MatchTokenType(ETokenType::IDENTIFIER);
		Type(parent);
		parent.children_.push_back(std::move(node));
		if(IsTokenSource(","))
		{
			MatchTokenSource(",");
			Parameters(parent);
		}
	}

	//// Parse a code block (with a generic type list) production
	//std::unique_ptr<Node> Parser::GenericCodeBlock()
	//{
	//	LOG_GRAMMAR("Generic Code Block");
	//	auto node = std::make_unique<BinaryOpNode>(ENodeType::GENERIC_BLOCK);
	//	node->left_ = TypeList(*node);
	//	if(IsTokenSource("["))
	//	{
	//		node->right_ = ParameterisedCodeBlock();
	//	}
	//	else if(IsTokenSource("{"))
	//	{
	//		node->right_ = CodeBlock();
	//	}
	//	else
	//	{
	//		throw BuildSyntaxInvalidException("code block");
	//	}
	//	return node;
	//}

	//// Parse a generic code block type list production
	//void Parser::TypeList(Node & node)
	//{
	//	LOG_GRAMMAR("Type List");
	//	auto node = std::make_unique<NodeListNode>(ENodeType::TYPE_LIST);
	//	MatchTokenSource("<");
	//	if(!IsTokenSource("<"))
	//	{
	//		auto node = std::make_unique<NodeListNode>(ENodeType::TYPE_LIST);
	//		Types(*node);
	//	}
	//	MatchTokenSource(">");
	//	return node;
	//}

	//// Parse a types production
	//void Parser::Types(NodeListNode & parent)
	//{
	//	LOG_GRAMMAR("Types");
	//	auto node = std::make_unique<IdentifierNode>(ENodeType::IDENTIFIER, token_stream_->GetLookahead().source_);
	//	Type(*node);
	//	parent.children_.push_back(std::move(node));
	//	if(IsTokenSource(","))
	//	{
	//		MatchTokenSource(",");
	//		Types(parent);
	//	}
	//}

	// Parse a return statement production
	std::unique_ptr<Node> Parser::ReturnStatement()
	{
		LOG_GRAMMAR("Return Statement");
		auto node = std::make_unique<UnaryOpNode>(ENodeType::RETURN_STATEMENT);
		MatchTokenSource("return");
		node->child_ = RValue();
		return node;
	}

	// Parse an expression
	std::unique_ptr<Node> Parser::Expression()
	{
		LOG_GRAMMAR("Expression");
		return BooleanOrExpression();
	}

	// Parse a boolean or expression production
	std::unique_ptr<Node> Parser::BooleanOrExpression()
	{
		LOG_GRAMMAR("Boolean Or Expression");
		auto node = BooleanAndExpression();
		if(IsTokenSource("or"))
		{
			auto left_node = std::move(node);
			auto node = std::make_unique<BinaryOpNode>(ENodeType::BOOLEAN_OR);
			node->left_ = std::move(left_node);
			MatchTokenSource("or");
			node->right_ = BooleanOrExpression();
			return node;
		}
		return node;
	}

	// Parse a boolean and expression production
	std::unique_ptr<Node> Parser::BooleanAndExpression()
	{
		LOG_GRAMMAR("Boolean And Expression");
		auto node = AdditionOrSubtractionExpression();
		if(IsTokenSource("and"))
		{
			auto left_node = std::move(node);
			auto node = std::make_unique<BinaryOpNode>(ENodeType::BOOLEAN_AND);
			node->left_ = std::move(left_node);
			MatchTokenSource("and");
			node->right_ = BooleanAndExpression();
			return node;
		}
		return node;
	}

	// Parse an addition or subtraction expression production
	std::unique_ptr<Node> Parser::AdditionOrSubtractionExpression()
	{
		LOG_GRAMMAR("Addition or Subtraction Expression");
		auto node = DivisionOrMultiplicationExpression();
		if(IsTokenSource("+"))
		{
			auto left_node = std::move(node);
			auto node = std::make_unique<BinaryOpNode>(ENodeType::ADDITION);
			node->left_ = std::move(left_node);
			MatchTokenSource("+");
			node->right_ = AdditionOrSubtractionExpression();
			return node;
		}
		else if(IsTokenSource("-"))
		{
			auto left_node = std::move(node);
			auto node = std::make_unique<BinaryOpNode>(ENodeType::SUBTRACTION);
			node->left_ = std::move(left_node);
			MatchTokenSource("-");
			node->right_ = AdditionOrSubtractionExpression();
			return node;
		}
		return node;
	}

	// Parse a division or multiplication expression production
	std::unique_ptr<Node> Parser::DivisionOrMultiplicationExpression()
	{
		LOG_GRAMMAR("Division or Multiplication Expression");
		auto node = FactorExpression();
		if(IsTokenSource("/"))
		{
			auto left_node = std::move(node);
			auto node = std::make_unique<BinaryOpNode>(ENodeType::DIVISION);
			node->left_ = std::move(left_node);
			MatchTokenSource("/");
			node->right_ = DivisionOrMultiplicationExpression();
			return node;
		}
		else if(IsTokenSource("*"))
		{
			auto left_node = std::move(node);
			auto node = std::make_unique<BinaryOpNode>(ENodeType::MULTIPLICATION);
			node->left_ = std::move(left_node);
			MatchTokenSource("*");
			node->right_ = DivisionOrMultiplicationExpression();
			return node;
		}
		return node;
	}

	// Parse a factor expression production
	std::unique_ptr<Node> Parser::FactorExpression()
	{
		LOG_GRAMMAR("Factor Expression");
		if(IsTokenType(ETokenType::NUMBER_LITERAL))
		{
			auto node = std::make_unique<RValIntegerLiteralNode>(ENodeType::RVAL_INTEGER_LITERAL);
			node->type_ = ENodeType::RVAL_INTEGER_LITERAL;
			node->value_ = token_stream_->GetLookahead().source_;		// TODO - Add support for floats
			MatchTokenType(ETokenType::NUMBER_LITERAL);
			return node;
		}
		else if(IsTokenType(ETokenType::STRING_LITERAL))
		{
			auto node = std::make_unique<RValStringLiteralNode>(ENodeType::RVAL_STRING_LITERAL);
			node->type_ = ENodeType::RVAL_STRING_LITERAL;
			node->string_ = token_stream_->GetLookahead().source_;		// TODO - Add support for floats
			MatchTokenType(ETokenType::STRING_LITERAL);
			return node;
		}
		else if(IsTokenSource("("))
		{
			MatchTokenSource("(");
			auto node = Expression();
			MatchTokenSource(")");
			return node;
		}
		else
		{
			// The expression may involve identifiers, dots, and calls
			return Call(nullptr);
		}
	}

	// Test the type of the lookahead
	bool Parser::IsTokenType(ETokenType type)
	{
		if(!token_stream_->IsLookaheadValid())
		{
			return false;
		}
		return token_stream_->GetLookahead().type_ == type;
	}

	// Test the source string of the lookahead
	bool Parser::IsTokenSource(std::string source)
	{
		if(!token_stream_->IsLookaheadValid())
		{
			return false;
		}
		return token_stream_->GetLookahead().source_ == source;
	}

	// Match a terminal token
	void Parser::MatchTokenType(ETokenType type)
	{
		if(token_stream_->IsLookaheadValid() && token_stream_->GetLookahead().type_ == type)
		{
			LOG_MATCH("Matched: " << ETokenTypeStrings[static_cast<uint32_t>(type)] << ", " << token_stream_->GetLookahead().source_);
			token_stream_->NextToken();
		}
		else
		{
			throw BuildSyntaxInvalidException(std::string("type ") + ETokenTypeStrings[static_cast<uint32_t>(type)]);
		}
	}

	// Match a terminal source string
	void Parser::MatchTokenSource(std::string source)
	{
		if(token_stream_->IsLookaheadValid() && token_stream_->GetLookahead().source_ == source)
		{
			LOG_MATCH("Matched: " << ETokenTypeStrings[static_cast<uint32_t>(token_stream_->GetLookahead().type_)] << ", " << source);
			token_stream_->NextToken();
		}
		else
		{
			throw BuildSyntaxInvalidException("source " + source);
		}
	}

	SyntaxInvalidException Parser::BuildSyntaxInvalidException(std::string_view expected)
	{
		std::string error_msg = std::string("Expected ") + expected.data() + ", got ";
		if(token_stream_->IsLookaheadValid())
		{
			error_msg += std::string("(")
				+ ETokenTypeStrings[static_cast<uint32_t>(token_stream_->GetLookahead().type_)]
				+ ": "
				+ token_stream_->GetLookahead().source_
				+ ")";
		}
		else
		{
			error_msg += "nothing (EOF)";
		}
		return SyntaxInvalidException(error_msg);
	}
}
