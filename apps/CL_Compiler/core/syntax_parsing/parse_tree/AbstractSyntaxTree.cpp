#include "stdafx.h"
#include "AbstractSyntaxTree.h"

namespace compiler
{
	AbstractSyntaxTree::AbstractSyntaxTree() : root_node_(ENodeType::STATEMENTS)
	{

	}

	AbstractSyntaxTree::~AbstractSyntaxTree()
	{

	}

	std::string AbstractSyntaxTree::ToString() const
	{
		std::string text { "" };
		std::vector<std::string> lines;

		auto stringify_index = [](int index) -> std::string {
			return std::to_string(index + 1); // +1 since most line numberers start at 1 rather than 0
		};

		std::function<int(Node const &, int)> parse_node = [&parse_node, &stringify_index, &lines](Node const & n, int my_index) -> int {
			// Generic node statement
			lines.push_back(ENodeTypeStrings[static_cast<size_t>(n.type_)]);
			size_t my_line_idx = lines.size() - 1;
			int next_child_index = my_index + 1;

			// Parse child nodes if this node has any children
			switch(n.type_)
			{
			case ENodeType::STATEMENTS:
			case ENodeType::BLOCK:
			case ENodeType::ARGUMENT_LIST:
			case ENodeType::PARAMETER_LIST:
			{
				for(std::unique_ptr<Node> const & c : static_cast<NodeListNode const &>(n).children_)
				{
					int this_child_index = next_child_index;
					next_child_index = parse_node(*c, this_child_index);
					lines[my_line_idx] += " " + stringify_index(this_child_index);
				}
			} break;
			case ENodeType::VARIABLE_ASSIGNMENT:
			case ENodeType::CONSTANT_ASSIGNMENT:
			case ENodeType::ADDITION:
			case ENodeType::SUBTRACTION:
			case ENodeType::MULTIPLICATION:
			case ENodeType::DIVISION:
			case ENodeType::BOOLEAN_AND:
			case ENodeType::BOOLEAN_OR:
			case ENodeType::CALL:
			case ENodeType::DOT:
			case ENodeType::PARAMETERISED_BLOCK:
			{
				auto const & p = static_cast<BinaryOpNode const &>(n);
				int left_index = next_child_index;
				if(p.left_)
				{
					next_child_index = parse_node(*p.left_, left_index);
					lines[my_line_idx] += " " + stringify_index(left_index);
				}
				int right_index = next_child_index;
				if(p.right_)
				{
					next_child_index = parse_node(*p.right_, right_index);
					lines[my_line_idx] += " " + stringify_index(right_index);
				}
			} break;
			case ENodeType::IDENTIFIER:
			{
				auto const & p = static_cast<IdentifierNode const &>(n);
				lines[my_line_idx] += " " + p.id_;
			} break;
			case ENodeType::RVAL_STRING_LITERAL:
			{
				auto const & p = static_cast<RValStringLiteralNode const &>(n);
				lines[my_line_idx] += " " + p.string_;
			} break;
			case ENodeType::RVAL_INTEGER_LITERAL:
			{
				auto const & p = static_cast<RValIntegerLiteralNode const &>(n);
				lines[my_line_idx] += " " + p.value_;
			} break;
			case ENodeType::RETURN_STATEMENT:
			{
				auto const & p = static_cast<UnaryOpNode const &>(n);
				int child_index = next_child_index;
				if(p.child_)
				{
					next_child_index = parse_node(*p.child_, child_index);
					lines[my_line_idx] += " " + stringify_index(child_index);
				}
			} break;
			}
			return next_child_index;
		};
		parse_node(root_node_, 0);
		for(auto const & line : lines)
		{
			text += line + "\n";
		}
		return text;
	}
}
