#pragma once

namespace compiler
{
	// !!!IMPORTANT!!! - If adding/removing from ENodeType, ensure ENodeTypeStrings is updated too!!!
	enum class ENodeType : size_t
	{
		STATEMENTS = 0,
		VARIABLE_ASSIGNMENT,
		CONSTANT_ASSIGNMENT,

		ADDITION,
		SUBTRACTION,
		MULTIPLICATION,
		DIVISION,

		BOOLEAN_AND,
		BOOLEAN_OR,

		DOT,
		OBJECT_VISIBLE,
		OBJECT_HIDDEN,
		TEMPLATE_VISIBLE,
		TEMPLATE_HIDDEN,

		CALL,
		BLOCK,
		PARAMETERISED_BLOCK,
		PARAMETER_LIST,
		ARGUMENT_LIST,
		GENERIC_BLOCK,
		TYPE_LIST,

		IDENTIFIER,

		RVAL_STRING_LITERAL,
		RVAL_INTEGER_LITERAL,
		RVAL_FLOAT_LITERAL,
		RVAL_NULL_LITERAL,
		RVAL_ARG_INDEXED,

		RETURN_STATEMENT,

		COUNT
	};

	constexpr char const * ENodeTypeStrings[] = {
		"Statements",
		"Variable Assignment",
		"Constant Assignment",

		"Addition",
		"Subtraction",
		"Multiplication",
		"Division",

		"Boolean_And",
		"Boolean_Or",

		"Dot",
		"Object_Visible",
		"Object_Hidden",
		"Template_Visible",
		"Template_Hidden",

		"Call",
		"Block",
		"Parameterised_Block",
		"Parameter_List",
		"Argument_List",
		"Generic_Block",
		"Type_List",

		"Identifier",

		"RVal_String_Literal",
		"RVal_Integer_Literal",
		"RVal_Float_Literal",
		"RVal_Null_Literal",
		"RVal_Arg_Indexed",

		"Return_Statement"
	};

	static_assert(sizeof ENodeTypeStrings / sizeof ENodeTypeStrings[0] == static_cast<long>(ENodeType::COUNT));
}
