#pragma once
#include "Node.h"

namespace compiler
{
	class AbstractSyntaxTree
	{
	public:
		AbstractSyntaxTree();
		~AbstractSyntaxTree();

		NodeListNode & GetRootNode() { return root_node_; }

		std::string ToString() const;

	private:
		NodeListNode root_node_;
	};
}
