#pragma once
#include "ENodeType.h"

namespace compiler
{
	struct Node
	{
		ENodeType type_;
		Node(ENodeType type) : type_(type) {}
		virtual ~Node() {}
	};

	// Data for a STATEMENTS/BLOCK/ARGUMENT_LIST/TYPE_LIST node
	struct NodeListNode final : public Node
	{
		std::vector<std::unique_ptr<Node>> children_;
		NodeListNode(ENodeType type) : Node(type) {}
	};

	// Data for a RETURN_STATEMENT/PRE_INCREMENT/POST_INCREMENT/PRE_DECREMENT/POST_DECREMENT
	struct UnaryOpNode final : public Node
	{
		std::unique_ptr<Node> child_;
		UnaryOpNode(ENodeType type) : Node(type) {}
	};

	// Data for an ASSIGNMENT/ADDITION/SUBTRACTION/MULTIPLICATION/DIVISION/CALL/DOT node
	struct BinaryOpNode final : public Node
	{
		std::unique_ptr<Node> left_;
		std::unique_ptr<Node> right_;
		BinaryOpNode(ENodeType type) : Node(type) {}
	};

	// Data for an RVAL_STRING_LITERAL node
	struct RValStringLiteralNode final : public Node
	{
		std::string string_;
		RValStringLiteralNode(ENodeType type) : Node(type) {}
	};

	// Data for an RVAL_INTEGER_LITERAL/RVAL_ARG_INDEXED node
	struct RValIntegerLiteralNode final : public Node
	{
		std::string value_;	// TODO - Change to int64_t
		///int64_t value_;
		RValIntegerLiteralNode(ENodeType type) : Node(type) {}
	};

	// Data for an RVAL_FLOAT_LITERAL node
	struct RValFloatLiteralNode final : public Node
	{
		double value_ { 0.0 };
		RValFloatLiteralNode(ENodeType type) : Node(type) {}
	};

	// Data for a NULL_LITERAL node
	struct RValNullLiteralNode final : public Node
	{
		RValNullLiteralNode(ENodeType type) : Node(type) {}
	};

	// Data for an IDENTIFIER node
	struct IdentifierNode final : public Node
	{
		std::string id_;
		//std::unique_ptr<Node> var_type_;
		IdentifierNode(ENodeType type, std::string const & id) : Node(type), id_(id) {}
	};
}
