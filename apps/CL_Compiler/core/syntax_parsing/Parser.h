#pragma once
#include "SyntaxInvalidException.h"
#include "core/lexing/TokenStream.h"

namespace compiler
{
	class AbstractSyntaxTree;

	struct Node;
	struct NodeListNode;
	struct BinaryOpNode;

	class Parser
	{
	public:
		Parser();
		~Parser();

		// Parse a stream of lexical units (tokens)
		// Constructs a parse tree
		// Throws if syntax is invalid
		std::unique_ptr<AbstractSyntaxTree> ParseTokenStream(TokenStream * token_stream);

	private:
		// Parse a statements production
		void Statements(NodeListNode & parent);

		// Parse a statement production
		std::unique_ptr<Node> Statement();

		// Parse zero or more consecutive new lines
		void NewLines();

		// Parse a field declaration production (declaration of a block/template field within a code block)
		std::unique_ptr<Node> FieldDeclaration();

		// Parse a local declaration/identifier expression
		std::unique_ptr<Node> LocalDeclarationOrIdentifierExpression();

		// Parse an rvalue production
		std::unique_ptr<Node> RValue();

		// Parse a type production
		void Type(Node & node);

		// Parse an assignment production
		std::unique_ptr<BinaryOpNode> Assignment();

		// Parse a dot production (some identifier dotted with another, e.g. a.b)
		std::unique_ptr<Node> Dot(std::unique_ptr<Node> lhs);

		// Parse a call production
		std::unique_ptr<Node> Call(std::unique_ptr<Node> lhs);

		// Parse an argument list production
		std::unique_ptr<Node> ArgumentList();

		// Parse an arguments production
		void Arguments(NodeListNode & parent);

		// Parse a code block production
		std::unique_ptr<Node> CodeBlock();

		// Parse a code block (with a parameter list) production
		std::unique_ptr<Node> ParameterisedCodeBlock();

		// Parse a code block parameter list production
		std::unique_ptr<Node> ParameterList();

		// Parse a parameters production
		void Parameters(NodeListNode & parent);

		// Parse a code block (with a generic type list) production
		//std::unique_ptr<Node> GenericCodeBlock();

		// Parse a type list production
		//void TypeList(Node & node);

		// Parse a types production
		//void Types(NodeListNode & parent);

		// Parse a return statement production
		std::unique_ptr<Node> ReturnStatement();

		// Parse an expression production
		std::unique_ptr<Node> Expression();

		// Parse a boolean or expression production
		std::unique_ptr<Node> BooleanOrExpression();

		// Parse a boolean and expression production
		std::unique_ptr<Node> BooleanAndExpression();

		// Parse a subtraction expression production
		std::unique_ptr<Node> AdditionOrSubtractionExpression();

		// Parse a multiplication expression production
		std::unique_ptr<Node> DivisionOrMultiplicationExpression();

		// Parse a factor expression production
		std::unique_ptr<Node> FactorExpression();

		// Test the type of the lookahead
		bool IsTokenType(ETokenType type);

		// Test the source string of the lookahead
		bool IsTokenSource(std::string source);

		// Match a terminal token
		void MatchTokenType(ETokenType type);

		// Match a terminal source string
		void MatchTokenSource(std::string source);

	private:
		// Build a syntax invalid exception from input parameters
		SyntaxInvalidException BuildSyntaxInvalidException(std::string_view expected);

	private:
		// The stream of tokens currently being parsed
		TokenStream * token_stream_ { nullptr };
	};
}
