#pragma once
#include <exception>

namespace compiler
{
	class SyntaxInvalidException : public std::exception
	{
	public:
		SyntaxInvalidException(std::string_view what)
			: std::exception(), what_(std::string("Syntax is invalid: ") + what.data()) {}

		virtual const char * what() const throw() { return what_.c_str(); }

	private:
		std::string what_;
	};
}
