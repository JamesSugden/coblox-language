#pragma once
#include "file_system/File.h"
#include "lexing/Lexer.h"
#include "syntax_parsing/Parser.h"
#include "semantic_analysis/Analyser.h"

namespace compiler
{
	class Compiler
	{
	public:
		Compiler();
		~Compiler();

		// Compile a Coblox program
		// File passed is the file containing the entry point to the program
		void CompileProgram(File const & f);

	private:
		// Lexer used to split program source into a stream of tokens
		Lexer lexer_;

		// Parser used to generate a parse tree from a stream of tokens
		Parser parser_;

		// Analyser used to perform semantic analysis on ast
		Analyser analyser_;
	};
}
