#pragma once

namespace compiler
{
	class AbstractSyntaxTree;

	class Analyser
	{
	public:
		Analyser();
		~Analyser();

		// Analyse an abstract syntax tree
		void AnalyseAst(AbstractSyntaxTree & ast);
	};
}
