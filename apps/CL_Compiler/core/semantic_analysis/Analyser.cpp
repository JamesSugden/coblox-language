#include "stdafx.h"
#include "Analyser.h"
#include "core/syntax_parsing/parse_tree/AbstractSyntaxTree.h"
#include "core/syntax_parsing/parse_tree/Node.h"
#include "core/syntax_parsing/parse_tree/ENodeType.h"
#include "core/symbols/SymbolTable.h"
#include "core/symbols/Symbol.h"
#include "core/symbols/Scope.h"

namespace compiler
{
	Analyser::Analyser()
	{

	}

	Analyser::~Analyser()
	{

	}

	// Analyse an abstract syntax tree
	void Analyser::AnalyseAst(AbstractSyntaxTree & ast)
	{
		std::cout << "\n============================";
		std::cout << "\n== Analysing AST";
		std::cout << "\n============================\n\n";

		SymbolTable symbol_table;

		std::function<void(Node const &, Scope *)> parse_node = [&parse_node, &symbol_table](Node const & n, Scope * scope) {
			// Parse child nodes if this node has any children
			Scope * current_scope = scope;
			switch(n.type_)
			{
			case ENodeType::BLOCK:
				//	current_scope = current_scope->AddScope();
			case ENodeType::STATEMENTS:
			case ENodeType::ARGUMENT_LIST:
			case ENodeType::PARAMETER_LIST:
			{
				for(std::unique_ptr<Node> const & c : static_cast<NodeListNode const &>(n).children_)
					parse_node(*c, current_scope);
			} break;
			case ENodeType::PARAMETERISED_BLOCK:
				//	current_scope = current_scope->AddScope();
			case ENodeType::ADDITION:
			case ENodeType::SUBTRACTION:
			case ENodeType::MULTIPLICATION:
			case ENodeType::DIVISION:
			case ENodeType::CALL:
			case ENodeType::DOT:
			{
				auto const & p = static_cast<BinaryOpNode const &>(n);
				if(p.left_)
					parse_node(*p.left_, current_scope);
				if(p.right_)
					parse_node(*p.right_, current_scope);
			} break;
			case ENodeType::VARIABLE_ASSIGNMENT:
			case ENodeType::CONSTANT_ASSIGNMENT:
			{
				auto const & p = static_cast<BinaryOpNode const &>(n);
				if(p.left_->type_ == ENodeType::IDENTIFIER)
				{
					auto const & i = static_cast<IdentifierNode const &>(*p.left_);
					Symbol * s;
					if(s = current_scope->Lookup(i.id_))
					{
						// Either assigning to an existing id in this scope or higher scope...
						std::cout << i.id_ << " exists in the current scope\n";
					}
					else if(s = current_scope->AddSymbol(i.id_))
					{
						// ...Or creating a new variable in the current scope
						std::cout << i.id_ << " added to current scope\n";
					}
				}
				else if(p.left_->type_ == ENodeType::DOT)
				{
					// If the left side of the dot operator is {+} or {-} and the right is an id, symbol should be in current scope
					auto const & d = static_cast<BinaryOpNode const &>(*p.left_);
					if((d.left_->type_ == ENodeType::OBJECT_HIDDEN || d.left_->type_ == ENodeType::OBJECT_VISIBLE)
						&& d.right_->type_ == ENodeType::IDENTIFIER)
					{
						auto const & i = static_cast<IdentifierNode const &>(*d.right_);
						Symbol * s;
						if(s = current_scope->LocalLookup(i.id_))
						{
							// Either assigning to an existing id in this scope or higher scope...
							std::cout << i.id_ << " exists in the current scope\n";
						}
						else if(s = current_scope->AddSymbol(i.id_))
						{
							// ...Or creating a new variable in the current scope
							std::cout << i.id_ << " added to current scope\n";
						}
					}

					// TODO - Find the symbol in the scope defined by the sequence of dot operation
				}

				parse_node(*p.right_, current_scope);
			} break;
			}
		};
		parse_node(ast.GetRootNode(), symbol_table.GetGlobalScope());
	}
}
