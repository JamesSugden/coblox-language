#pragma once
#include "ETokenType.h"

namespace compiler
{
	struct Token
	{
		ETokenType type_;
		std::string source_;

		Token(ETokenType type, std::string const & source)
			: type_(type), source_(source) {}
	};
}
