#pragma once
#include "Token.h"
#include "TokenStream.h"

namespace compiler
{
	class Lexer
	{
	public:
		Lexer();
		~Lexer();

		// Get the token stream from a string where each token represents a lexical unit
		std::unique_ptr<TokenStream> GetTokenStream(std::string const & source);
	};
}
