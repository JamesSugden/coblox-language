#include "stdafx.h"
#include "Lexer.h"
#include "ETokenType.h"

namespace compiler
{
	Lexer::Lexer()
	{

	}

	Lexer::~Lexer()
	{

	}

	// Get the token stream from a string where each token represents a lexical unit
	std::unique_ptr<TokenStream> Lexer::GetTokenStream(std::string const & source)
	{
		std::cout << "\n============================";
		std::cout << "\n== Lexing Tokens";
		std::cout << "\n============================\n\n";

		std::unique_ptr<TokenStream> stream = std::make_unique<TokenStream>();

		std::string captureKeyword = "(\\{-\\}|\\{\\+\\}|<->|<\\+>|\\?|and|or|true|false|return|null)";
		std::string captureNumber = "(\\b[0-9]+(?:\\.[0-9]+)?\\b)";
		std::string captureLineComment = "(\\/\\/.*)";
		std::string captureType = "(int|float|bool|string|byte)";
		std::string captureId = "(\\b[a-zA-Z][a-zA-Z_0-9]*\\b)";
		std::string captureString = "((?:\".*?\")|(?:\'.*?\'))";
		std::string captureBinaryOp = "(\\+=|-=|\\*=|/=|%=|=|<-|\\+|-|\\*|/|\\*\\*|%|\\.)";
		std::string captureUnaryOp = "(\\+\\+|--)";
		std::string captureDelims = "(,|\\(|\\)|\\[|\\]|<|>|:|\\{(?!-)|\\{(?!\\+)|(?!-)\\}|(?!\\+)\\})";
		std::string captureNewline = "(\\n|\\r|\\r\\n|\\n\\r)";
		std::string captureJunk = "([^\\s\\x00]+)";

		// Order of capture is important, e.g. keywords and types must be captured before identifiers
		std::regex r(captureKeyword + "|" + captureNumber + "|" + captureLineComment + "|"
			+ captureType + "|" + captureId + "|" + captureString + "|" + captureBinaryOp + "|"
			+ captureUnaryOp + "|" + captureDelims + "|" + captureNewline + "|" + captureJunk
			, std::regex_constants::ECMAScript);

		auto begin = std::sregex_iterator(source.begin(), source.end(), r);
		auto end = std::sregex_iterator();

		for(std::sregex_iterator it = begin; it != end; ++it)
		{
			std::smatch match = *it;

			// Find the group the string matched with
			for(auto index = 1; index < match.size(); index++)
			{
				if(!match[index].str().empty())
				{
					// If junk characters are found, the syntax must be invalid
					if(index == match.size() - 1)
					{
						std::cerr << "Syntax is invalid\n";
						return nullptr;
					}
					// Don't add comments to the stream since they are irrelevant to the compilation
					ETokenType type { (ETokenType)index };
					if(type != ETokenType::LINE_COMMENT)
					{
						stream->PushToken(type, match[index].str());
						std::cout << (int)type << ": " << match[index].str() << "\n";
					}
				}
			}
		}

		return stream;
	}
}
