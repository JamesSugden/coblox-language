#pragma once
#include "Token.h"

namespace compiler
{
	class TokenStream
	{
	public:
		TokenStream();
		virtual ~TokenStream();

		// Add a new token to the end of the stream
		template <typename... Args>
		void PushToken(Args &&... args)
		{
			tokens_.emplace_back(std::forward<Args>(args)...);
		}

		// Returns true iff there is at least one token in the stream
		bool HasFirstToken();

		// Returns true iff the current lookeahed is valid
		// Returns false if the stream has been exhausted
		bool IsLookaheadValid();

		// Advance the token stream
		// Returns true if there is a next token
		// Returns false if the end of the stream has been reached
		bool NextToken();

		// Get a reference to the lookahead token
		// i.e. the next token to parse
		inline Token const & GetLookahead() const { return tokens_[stream_index_]; }

	private:
		std::vector<Token> tokens_;
		int stream_index_ { 0 };
	};
}
