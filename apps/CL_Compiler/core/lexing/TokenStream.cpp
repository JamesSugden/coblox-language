#include "stdafx.h"
#include "TokenStream.h"

namespace compiler
{
	TokenStream::TokenStream()
	{

	}

	TokenStream::~TokenStream()
	{

	}

	// Returns true iff there is at least one token in the stream
	bool TokenStream::HasFirstToken()
	{
		return tokens_.size() >= 1;
	}

	// Returns true iff the current lookeahed is valid
	// Returns false if the stream has been exhausted
	bool TokenStream::IsLookaheadValid()
	{
		return stream_index_ < tokens_.size();
	}

	// Advance the token stream
	// Returns true if there is a next token
	// Returns false if the end of the stream has been reached
	bool TokenStream::NextToken()
	{
		stream_index_++;
		return stream_index_ < tokens_.size();
	}
}
