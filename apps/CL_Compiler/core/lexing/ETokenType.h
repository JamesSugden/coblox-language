#pragma once

namespace compiler
{
	enum class ETokenType : uint32_t
	{
		KEYWORD = 1,
		NUMBER_LITERAL,
		LINE_COMMENT,
		NATIVE_TYPE,
		IDENTIFIER,
		STRING_LITERAL,
		BINARY_OP,
		UNARY_OP,
		DELIM,
		NEWLINE
	};

	constexpr char const * ETokenTypeStrings[] = {
		"N/A",
		"Keyword",
		"Number Literal",
		"Line Comment",
		"Native Type",
		"Identifier",
		"String Literal",
		"Binary Operator",
		"Unary Operator",
		"Delimiter",
		"New Line"
	};
}
