import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from graphviz import Digraph

# Plot the graph g and display in a window
def plot(g):
    g.render('output.gv', view=True)

# Load text file and create graph object
def loadGraph(path):
    #g = nx.MultiGraph()
    g = Digraph(comment='AST')
    with open(path, 'r') as f:
        text = f.read()
        # Interpret each line as a vertex
        lines = text.splitlines()
        node_names = dict()
        for l in range(len(lines)):
            line = lines[l]
            splits = line.split()
            first_edge = 0
            if len(splits) > 0:
                name = splits[0]
                if name == 'Identifier' or name.startswith('RVal'):
                    id = splits[1]
                    g.node(str(l), id)
                else:
                    g.node(str(l), name)
                    for i in range(1, len(splits)):
                        child = int(splits[i])
                        g.edge(str(l), str(child), constraint='true')
    return g

def main():
    # Load the graph file
    g = loadGraph('E:/Coblox/AST.txt')
    # Plot the graph
    plot(g)

if __name__ == '__main__':
    main()
