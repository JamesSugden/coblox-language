#pragma once
#include <string.h>

// Concatenate 2 strings a and b and store in string c
// Strings are c-style null terminated strings
inline void std_strings_concat(char * a, char * b, char * c)
{
	strcpy(c, a);
	strcat(c, b);
}
