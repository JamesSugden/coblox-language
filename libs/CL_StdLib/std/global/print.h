#pragma once
#include <stdio.h>
#include <stdarg.h>

// Print a null-terminated string
// TODO - Allow comma separated arguments
inline void std_global_print(char const * string)
{
	printf(string);
}

// Print a null-terminated string followed by a newline
// TODO - Allow comma separated arguments
inline void std_global_println(char const * string)
{
#ifdef _WIN32
	printf("%s\r\n", string);
#else
	printf("%s\n", string);
#endif
}

// Print a user formatted null-terminated string
// Internally passes arguments directly onto printf
inline void std_global_printf(char const * format, ...)
{
	va_list arglist;
	va_start(arglist, format);
	vprintf(format, arglist);
	va_end(arglist);
}

// Print a user formatted null-terminated string
// Internally passes arguments directly onto fprintf
// TODO
